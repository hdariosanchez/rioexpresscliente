import 'dart:async';
import 'package:location/location.dart';
import 'package:rioexpresscliente/models/userLocation.model.dart';

class LocationService {
  // Keep track of current Location
  UserLocation _currentLocation;
  Location location = Location();
  // Continuously emit location updates
  StreamController<UserLocation> _locationController =
      StreamController<UserLocation>.broadcast();

  LocationService() {
    location.requestPermission().then((granted) {
      if (granted != null) {
        location.onLocationChanged().listen((locationData) {
          if (locationData != null) {
            _locationController.add(UserLocation(
              latitude: locationData.latitude,
              longitude: locationData.longitude,
            ));
          }
        });
      }
    });
  }
  Stream<UserLocation> get locationStream => _locationController.stream;
//-----------------------------------
  Location _locationService = new Location();
  bool _permission = false;
  var localizationCurrent = null;
  var error = null;
  //var location = Location();
  //-----------------------------------

  Future<UserLocation> getLocation() async {
    //LocationData userLocation = null;
    try {
      bool serviceStatus = await _locationService.serviceEnabled();
      print("Service status Location plugin --> : $serviceStatus");
      if (serviceStatus) {
        _permission = (await _locationService.requestPermission()) as bool;
        print("Permission: $_permission");
        if (_permission) {
          var userLocation = await location.getLocation();
          _currentLocation = UserLocation(
            latitude: userLocation.latitude,
            longitude: userLocation.longitude,
          );
          error = null;
        }
      } else {
        _permission = (await _locationService.requestPermission()) as bool;
        print("Permission: $_permission");
      }
    } catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'Permission denied';
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error =
            'Permission denied - please ask the user to enable it from the app settings';
      }
      location = null;
    }
    return _currentLocation;
  }
}
