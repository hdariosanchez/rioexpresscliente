import 'package:rioexpresscliente/bloc/blocCarritoCompra.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rioexpresscliente/bloc/carrito.bloc.dart';
import 'package:rioexpresscliente/bloc/productoNegocio.bloc.dart';
import 'package:rioexpresscliente/components/itemEspecificacion/itemDescripcion.component.dart';
import 'package:rioexpresscliente/components/itemEspecificacion/itemTitle.component.dart';
import 'package:rioexpresscliente/configuration/local.dart';

//IMPORTANDO MODELO DE NEGOCIOS
import 'package:rioexpresscliente/models/negocio.model.dart';
//IMPORTANDO PATRON BLOC DEL NEGOCIO
import 'package:rioexpresscliente/bloc/negocio.bloc.dart';
//IMPORTANDO COMPONENTES VIEWS
import 'package:rioexpresscliente/components/itemEspecificacion/itemGrid.component.dart';
//IMPORTANDO COMPONENTE DE GEOLOCATOR
import 'package:geolocator/geolocator.dart';
import 'package:rioexpresscliente/pages/carritoCompra.ui.dart';
import 'package:rioexpresscliente/pages/productoNegocio.ui.dart';

class NegocioItems extends StatefulWidget {
  NegocioItems({Key key}) : super(key: key);
  //final String tipoNegocio;

  @override
  _NegocioItemsState createState() => _NegocioItemsState();
}

class _NegocioItemsState extends State<NegocioItems> {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  Position _currentPosition;
  String _currentAddress;
  double _distanciaMetros;
  static IpConfig local = new IpConfig("192.168.1.15", "5000");
  //static IpConfig local = new IpConfig("35.247.235.167", "5000");

  void checkPermission() {
    geolocator.checkGeolocationPermissionStatus().then((status) {
      print('status: $status');
    });
    geolocator
        .checkGeolocationPermissionStatus(
            locationPermission: GeolocationPermission.locationAlways)
        .then((status) {
      print('always status: $status');
    });
    geolocator.checkGeolocationPermissionStatus(
        locationPermission: GeolocationPermission.locationWhenInUse)
      ..then((status) {
        print('whenInUse status: $status');
      });
  }

  Future<Position> _getLocation() async {
    var currentLocation;
    try {
      currentLocation = await geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.best);
    } catch (e) {
      currentLocation = null;
    }
    return currentLocation;
  }

  @override
  void initState() {
    super.initState();
    negocioBloc.fetchAllNegocio("");
    //ordenBloc.fetchAllOrdenProduct();

    LocationOptions locationOptions =
        LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 1);
    checkPermission();
    /*
    _getLocation().then((position) {
      _currentPosition = position;
    });
  */
  }

/*
  @override
  void dispose() {
    super.dispose();
    //negocioBloc.dispose();
    //bloc.dispose();
  }

  */

  Widget build(BuildContext context) {
    //final bloc2 = Provider.of<carritoCompraBloc>(context);
    //final totalCount = bloc2.cambia;
    return Scaffold(
      appBar: AppBar(
        title: Text("Productos"),
        actions: <Widget>[
          /*
          new IconButton(
            icon: new Icon(
              Icons.label,
              color: Colors.blue[800],
            ),
            onPressed: () {},
          ),
          new Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Container(
                height: 150.0,
                width: 30.0,
                child: new GestureDetector(
                  onTap: () {
                    //Navigator.pushNamed(context, '/carritoCompra');
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CarritoCompra(),
                      ),
                    );
                  },
                  child: new Stack(
                    children: <Widget>[
                      new IconButton(
                        icon: new Icon(
                          Icons.shopping_cart,
                          color: Colors.blue[800],
                        ),
                        onPressed: null,
                      ),
                      new Positioned(
                          child: new Stack(
                        children: <Widget>[
                          new Icon(Icons.brightness_1,
                              size: 22.0, color: Colors.red[700]),
                          new Positioned(
                              top: 4.0,
                              right: 5,
                              child: new Center(
                                child: StreamBuilder<int>(
                                    stream: ordenBloc.getNumberCar,
                                    initialData: 0,
                                    builder: (BuildContext context,
                                        AsyncSnapshot<int> snapshot) {
                                      return Text(
                                        '${snapshot.data}',
                                        style: new TextStyle(
                                            color: Colors.white,
                                            fontSize: 12.0,
                                            fontWeight: FontWeight.w500),
                                      );
                                    }),
                              )),
                        ],
                      )),

                    ],
                  ),
                )),
          )
          */
        ],
      ),
      //body: NegocioItemsWidget(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          StreamBuilder(
            stream: negocioBloc.allNegocio,
            builder: (context, AsyncSnapshot<NegocioModelList> snapshot) {
              if (snapshot.data != null) {
                return buildList2(snapshot, context);
              }
              /*else if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }*/
              return Center(
                child: Text("No existe negocio de esta categoria"),
              ); //Center(child: CircularProgressIndicator());
            },
          ),
        ],
      ),
    );
  }

  Widget buildList2(
      AsyncSnapshot<NegocioModelList> snapshot, BuildContext context) {
    return new Expanded(
      child: new ListView.builder(
        itemCount: snapshot.data.results.length,
        itemBuilder: (context, int index) {
          return InkResponse(
            child: Card(
              child: Stack(
                children: <Widget>[
                  snapshot.data.results[index].urlImagen != null
                      ? Image.network(
                          'http://${local.getIp()}:${local.getPort()}/imagenNegocio/${snapshot.data.results[index].id}${snapshot.data.results[index].urlImagen}.jpg')
                      : CircularProgressIndicator(),
                  Positioned(
                    top: 5,
                    left: 20,
                    child: Row(
                      children: <Widget>[
                        //Icon(Icons.arrow_forward_ios,size: 25, color: Colors.white),
                        SizedBox(
                            width: MediaQuery.of(context).size.width * .85),
                        //Icon(Icons.create, size: 25, color: Colors.white),
                        //SizedBox(width: 10),
                        Icon(Icons.more_vert, size: 25, color: Colors.white),
                      ],
                    ),
                  ),
                  new Positioned(
                    bottom: 20.0,
                    left: 10.0,
                    right: 10.0,
                    child: Card(
                      color: Colors.transparent,
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(1.0),
                            child: TitleEspecification(
                                1, snapshot.data.results[index].nombreCorto),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(1.0),
                            child: ItemDescriptionProduct("0001",
                                snapshot.data.results[index].descripcionLocal),
                          ),
                          /*
                          Padding(
                            padding: const EdgeInsets.all(1.0),
                            child: ItemDescriptionProduct(
                                "0001",
                                _currentPosition != null
                                    ? "Estas son coordenadas actuales ${_currentPosition.latitude}, LNG: ${_currentPosition.longitude} -- " +
                                        _currentAddress
                                    : "Estas son coordenadas del negocio ${snapshot.data.results[index].latitud}, - ${snapshot.data.results[index].longitud}"),
                          ),
                          */
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 2,
                    left: 20,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 6.0),
                          child: Icon(Icons.motorcycle,
                              size: 25, color: Colors.white),
                        ),
                        Text("\$2\,50", //_currentAddress,
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                            ),
                            textAlign: TextAlign.justify,
                            textDirection: TextDirection.rtl),
                        SizedBox(
                            width: MediaQuery.of(context).size.width * .50),
                        Icon(Icons.location_on, size: 25, color: Colors.white),

                        Text(
                          "23 Km", //_currentAddress,
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                          ),
                          textAlign: TextAlign.justify,
                        ),
                        //SizedBox(width: 10),
                        //Icon(Icons.more_vert, size: 32, color: Colors.white),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            onTap: () {
              productoBloc.switchNegocio(snapshot.data.results[index]);
              //Navigator.pushNamed(context, '/negocioProductos');
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => NegocioProductosItems()),
              );

              /*
              Navigator.pushNamed(context, '/negocioProductos',
                  arguments: NegocioProductosItems(
                      //negocioWithServices: snapshot.data.results[index]),
                      ));
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => NegocioProductosItems(
                      negocioWithServices: snapshot.data.results[index]),
                ),
              );
              */
              //print('http://35.247.235.167:5000/imagenNegocio/${snapshot.data.results[index].id}${snapshot.data.results[index].urlImagen}.jpg'),
              /*
              for (var i = 0;
                  i < snapshot.data.results[index].servicioNegocio.length;
                  i++)
                {
                  print(
                      '${snapshot.data.results[index].servicioNegocio[i].id} -- ${snapshot.data.results[index].servicioNegocio[i].nombre}')
                }
                */
              /*
              _getCurrentLocation(snapshot.data.results[index].latitud,
                  snapshot.data.results[index].longitud)
                  */
            },
          );
        },
      ),
    );
  }
/*
openDetailPage(NegocioModelList data, int index) {
  final page = MovieDetailBlocProvider(
    child: MovieDetail(
      title: data.results[index].title,
      posterUrl: data.results[index].backdrop_path,
      description: data.results[index].overview,
      releaseDate: data.results[index].release_date,
      voteAverage: data.results[index].vote_average.toString(),
      movieId: data.results[index].id,
    ),
  );
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) {
      return page;
    }),
  );
}
*/

  _getCurrentLocation(double latitudNegocio, double longitudNegocio) async {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng(latitudNegocio, longitudNegocio,
          _currentPosition.latitude, _currentPosition.longitude);
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng(double latitudNegocio, double longitudNegocio,
      double latitudNegocioEnd, double longitudNegocioEnd) async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);
      double distanceInMeters = await Geolocator().distanceBetween(
          latitudNegocio,
          longitudNegocio,
          latitudNegocioEnd,
          longitudNegocioEnd);

      Placemark place = p[0];

      setState(() {
        _distanciaMetros = distanceInMeters;
        _currentAddress = place.locality;
      });
    } catch (e) {
      print(e);
    }
  }
}

/*
class NegocioItemsWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return StreamBuilder(
      initialData: bloc.allItems,
      stream: bloc.getStream,
      builder: (context, snapshot) {
        return snapshot.data["shop_items"].length > 0
            ? shopItemsListBuilder(snapshot, context)
            : Center(child: Text("Todos los productos han sido agotados"));
      },
    );
  }
}


Widget shopItemsListBuilder(snapshot, context) {
final bloc3 = Provider.of<carritoCompraBloc>(context);
  return ListView.builder(
    itemCount: snapshot.data["shop_items"].length,
    itemBuilder: (BuildContext context, i) {
      final shopList = snapshot.data["shop_items"];
      return ListTile(
        title: Text(shopList[i]['name']),
        subtitle: Text("\$${shopList[i]['price']}"),
        trailing: IconButton(
          icon: Icon(Icons.add_shopping_cart),
          onPressed: () {
            bloc3.cambia = 1;
            bloc.addToCart(shopList[i]);
          },
        ),
        onTap: () {},
      );
    },
  );
}
*/
