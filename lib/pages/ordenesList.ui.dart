import 'package:flutter/material.dart';
import 'package:rioexpresscliente/bloc/ordenes.bloc.dart';
import 'package:rioexpresscliente/components/buttons/circle_button.dart';
import 'package:rioexpresscliente/models/ordenFactura.model.dart';
import 'package:rioexpresscliente/pages/menuNegocios.ui.dart';
import 'package:rioexpresscliente/pages/productosList.ui.dart';
import 'package:rioexpresscliente/pages/seguimientoOrrden.ui.dart';
//import 'package:rioexpress_repartidor/view/productosList.ui.dart';
import 'package:shared_preferences/shared_preferences.dart';

//import 'login.ui.dart';

class OrdenesList extends StatefulWidget {
  OrdenesList({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _OrdenesListState createState() => _OrdenesListState();
}

class _OrdenesListState extends State<OrdenesList> {
  @override
  void initState() {
    super.initState();
    ordenesFactura.fetchAllOrdenesPendientes();
  }

  SharedPreferences sharedPreferences;

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        title: Text("Mis Ordenes"),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            StreamBuilder(
              stream: ordenesFactura.allMyOrdensPendiente,
              builder:
                  (context, AsyncSnapshot<OrdenFacturaModelList> snapshot) {
                if (snapshot.hasData) {
                  return buildList2(snapshot);
                } else if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                }
                return Text("No hay datos disponibles");
              },
            )
          ],
        ),
      ),
    );
  }

  Widget buildList2(AsyncSnapshot<OrdenFacturaModelList> snapshot) {
    return new Expanded(
      child: new ListView.builder(
        itemCount: snapshot.data.results.length,
        itemBuilder: (BuildContext ctxt, int index) {
          return InkResponse(
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: Colors.white,
                      ),
                      child: ListTile(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ProductosList(
                                productosList:
                                    snapshot.data.results[index].arrayProducto,
                              ),
                            ),
                          );

                          //print("Ecima");
                        },
                        leading: CircleAvatar(
                          child: Icon(Icons.timer),
                        ),
                        title: Text(
                          "${snapshot.data.results[index].direccionEntrega}",
                          style: TextStyle(
                            color: Theme.of(context).primaryColorDark,
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                        ),
                        subtitle: Text(
                          "Cel Principal: ${snapshot.data.results[index].celular} Cel Referencia ${snapshot.data.results[index].celularAdicional}",
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                          ),
                        ),
                        trailing: CircleButton(
                          size: 40,
                          padding: 1,
                          icon: Icons.directions,
                          iconColor: Colors.red,
                          onTap: () => {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (ctxt) => SeguimientoOrden(),
                              ),
                            )

                            /*
                            showDialog(
                              context: context,
                              builder: (BuildContext context) => Dialog(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        10.0)), //this right here
                                child: Container(
                                  height: 190,
                                  child: Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Center(
                                          child: Image.asset(
                                            'assets/image/icon_delete.png',
                                            width: 50.0,
                                          ),
                                        ),
                                        Text(
                                          "Esta seguro de eliminar su lugar ${snapshot.data.results[index].direccionEntrega}?",
                                          style: new TextStyle(
                                              color: Colors.black,
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: RaisedButton(
                                                  onPressed: () {
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text(
                                                    "NO",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                  color:
                                                      const Color(0xFFe27468),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: RaisedButton(
                                                  onPressed: () {
                                                    ordenesFactura
                                                        .deleteMyDirection(
                                                            snapshot
                                                                .data
                                                                .results[index]
                                                                .id);
                                                    ordenesFactura
                                                        .deleteMyDirectionBool
                                                        .listen(
                                                      (event) {
                                                        /*
                                                        event
                                                            ? direccionBloc
                                                                .fetchAllMyDirections()
                                                            : print(
                                                                'NO elimino direccion-->$event');
                                                                */
                                                      },
                                                    );
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text(
                                                    "Si",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                  color:
                                                      const Color(0xFF1BC0C5),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            */
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            onTap: () {
              print("Hola mundo ${snapshot.data.results[index].idCliente}");
            },
          );
        },
      ),
    );
  }
}
