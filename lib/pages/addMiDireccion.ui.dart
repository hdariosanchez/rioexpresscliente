//IMPORTANDO ARCHIVOS PARA EL MAPA CON GOOGLE MAP AND PICKER
import 'package:rioexpresscliente/google_maps_place_picker.dart';
//import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:flutter/material.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class AddMiDireccion extends StatefulWidget {
  @override
  _AddMiDireccionState createState() => _AddMiDireccionState();
}

class _AddMiDireccionState extends State<AddMiDireccion> {
  bool _isLoading = false;

  PickResult selectedPlace;
  static final kInitialPosition = LatLng(-1.666467, -78.659458);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      key: _scaffoldState,
      body: Stack(
        children: <Widget>[
          PlacePicker(
            apiKey: "AIzaSyDyKzp8s8uahdnHfuubdKuVkMxNqHwyDwc", //APIKeys.apiKey,
            initialPosition: kInitialPosition,
            useCurrentLocation: true,
            usePlaceDetailSearch: true,
            onPlacePicked: (result) {
              selectedPlace = result;
              print('viendo al result--> $result');
              //Navigator.of(context).pop();
              setState(() {});
            },
            //forceSearchOnZoomChanged: true,
            //automaticallyImplyAppBarLeading: false,
            //autocompleteLanguage: "ko",
            //region: 'au',
            //selectInitialPosition: true,
            // selectedPlaceWidgetBuilder: (_, selectedPlace, state, isSearchBarFocused) {
            //   print("state: $state, isSearchBarFocused: $isSearchBarFocused");
            //   return isSearchBarFocused
            //       ? Container()
            //       : FloatingCard(
            //           bottomPosition: 0.0,    // MediaQuery.of(context) will cause rebuild. See MediaQuery document for the information.
            //           leftPosition: 0.0,
            //           rightPosition: 0.0,
            //           width: 500,
            //           borderRadius: BorderRadius.circular(12.0),
            //           child: state == SearchingState.Searching
            //               ? Center(child: CircularProgressIndicator())
            //               : RaisedButton(
            //                   child: Text("Pick Here"),
            //                   onPressed: () {
            //                     // IMPORTANT: You MUST manage selectedPlace data yourself as using this build will not invoke onPlacePicker as
            //                     //            this will override default 'Select here' Button.
            //                     print("do something with [selectedPlace] data");
            //                     Navigator.of(context).pop();
            //                   },
            //                 ),
            //         );
            // },
            // pinBuilder: (context, state) {
            //   if (state == PinState.Idle) {
            //     return Icon(Icons.favorite_border);
            //   } else {
            //     return Icon(Icons.favorite);
            //   }
            // },
          ),
          _isLoading
              ? Stack(
                  children: <Widget>[
                    Opacity(
                      opacity: 0.3,
                      child: ModalBarrier(
                        dismissible: false,
                        color: Colors.grey,
                      ),
                    ),
                    Center(
                      child: Text("Espere---"),
                    ),
                  ],
                )
              : Container(),
        ],
      ),
    );
  }
}
