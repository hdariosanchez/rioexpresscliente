import 'package:flutter/material.dart';
import 'package:rioexpresscliente/bloc/direccion.bloc.dart';
import 'package:rioexpresscliente/components/buttons/circle_button.dart';
import 'package:rioexpresscliente/models/direccion.model.dart';
import 'package:rioexpresscliente/pages/addMiDireccion.ui.dart';

class MiDireccionP extends StatefulWidget {
  MiDireccionP({Key key}) : super(key: key);

  @override
  _MiDireccionPState createState() => _MiDireccionPState();
}

class _MiDireccionPState extends State<MiDireccionP> {
  @override
  void initState() {
    super.initState();
    direccionBloc.fetchAllMyDirections();
    direccionBloc.paginaOrdenRapida(false);
    /*
    LocationOptions locationOptions =
        LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 1);
    checkPermission();
    _getLocation().then((position) {
      _currentPosition = position;
    });
    */
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        title: Text("Mis Direcciones"),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            StreamBuilder(
              stream: direccionBloc.allMyDirections,
              builder: (context, AsyncSnapshot<MiDireccionModelList> snapshot) {
                if (snapshot.hasData) {
                  return buildList2(snapshot);
                } else if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                }
                return Center(child: CircularProgressIndicator());
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new FlatButton.icon(
                    // Un icono puede recibir muchos atributos, aqui solo usaremos icono, tamaño y color
                    icon: const Icon(
                      Icons.add_location,
                      size: 25.0,
                      color: Colors.white,
                    ),
                    label: const Text(
                      'Agregar direccion',
                      style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                    color: Colors.pinkAccent,
                    // Esto mostrara 'Me encanta' por la terminal
                    onPressed: () {
                      Navigator.pushNamed(context, '/addMiDireccion');
                      /*
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (ctxt) => AddMiDireccion(),
                        ),
                      );
                      */
                    },
                  ),
                ),
                //SizedBox(width: 10),
                //Icon(Icons.more_vert, size: 32, color: Colors.white),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildList2(AsyncSnapshot<MiDireccionModelList> snapshot) {
    return new Expanded(
      child: new ListView.builder(
        itemCount: snapshot.data.results.length,
        itemBuilder: (BuildContext ctxt, int index) {
          return InkResponse(
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: Colors.white,
                      ),
                      child: ListTile(
                        onTap: () {},
                        leading: CircleAvatar(
                          child: Icon(Icons.add_location),
                        ),
                        title: Text(
                          "${snapshot.data.results[index].nombre}",
                          style: TextStyle(
                            color: Theme.of(context).primaryColorDark,
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                        ),
                        subtitle: Text(
                          "${snapshot.data.results[index].latitud} , ${snapshot.data.results[index].longitud}",
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                          ),
                        ),
                        trailing: CircleButton(
                          size: 20,
                          padding: 15,
                          icon: Icons.delete,
                          iconColor: Colors.red,
                          onTap: () => {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) => Dialog(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        10.0)), //this right here
                                child: Container(
                                  height: 190,
                                  child: Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Center(
                                          child: Image.asset(
                                            'assets/image/icon_delete.png',
                                            width: 50.0,
                                          ),
                                        ),
                                        Text(
                                          "Esta seguro de eliminar su lugar ${snapshot.data.results[index].nombre}?",
                                          style: new TextStyle(
                                              color: Colors.black,
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: RaisedButton(
                                                  onPressed: () {
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text(
                                                    "NO",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                  color:
                                                      const Color(0xFFe27468),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: RaisedButton(
                                                  onPressed: () {
                                                    direccionBloc
                                                        .deleteMyDirection(
                                                            snapshot
                                                                .data
                                                                .results[index]
                                                                .id);
                                                    direccionBloc
                                                        .deleteMyDirectionBool
                                                        .listen(
                                                      (event) {
                                                        event
                                                            ? direccionBloc
                                                                .fetchAllMyDirections()
                                                            : print(
                                                                'NO elimino direccion-->$event');
                                                      },
                                                    );
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text(
                                                    "Si",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                  color:
                                                      const Color(0xFF1BC0C5),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          },
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    Text("${snapshot.data.results[index].idCliente}"),
                  ],
                ),
              ),
            ),
            onTap: () {
              print("Hola mundo ${snapshot.data.results[index].idCliente}");
            },
          );
        },
      ),
    );
  }
}
