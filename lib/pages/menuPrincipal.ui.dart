import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:rioexpresscliente/bloc/carrito.bloc.dart';
import 'package:rioexpresscliente/pages/addMiDireccion.ui.dart';
import 'package:rioexpresscliente/pages/carritoCompra.ui.dart';
import 'package:rioexpresscliente/pages/direccionList.ui.dart';
import 'package:rioexpresscliente/pages/login_page.ui.dart';
import 'package:rioexpresscliente/pages/principal.dart';
import 'package:rioexpresscliente/pages/prueba.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ordenesList.ui.dart';

class MenuPrincipal extends StatefulWidget {
  @override
  _MenuPrincipalState createState() => new _MenuPrincipalState();
}

class _MenuPrincipalState extends State<MenuPrincipal> {
  SharedPreferences sharedPreferences;

  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  logout() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  final List<Widget> _children = [
    Principal(title: "Profile Page"),
    OrdenesList(title: "Home Page"),
    CarritoCompra(),
    MiDireccionP()
  ];

  void handleClick(String value) {
    switch (value) {
      case 'Logout':
        break;
      case 'Settings':
        break;
    }
  }

  @override
  void initState() {
    super.initState();
    ordenBloc.fetchAllOrdenProduct();
    //logout();
  }

  List<String> imageLinks = [
    'https://homepages.cae.wisc.edu/~ece533/images/fruits.png',
    'https://homepages.cae.wisc.edu/~ece533/images/cat.png',
    'https://homepages.cae.wisc.edu/~ece533/images/peppers.png',
    'https://homepages.cae.wisc.edu/~ece533/images/tulips.png'
  ];
  int _count = 0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light().copyWith(
        primaryColor: Colors.yellow,
        accentColor: Colors.amber,
      ),
      home: Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        drawer: MenuLateral(),
        appBar: AppBar(
          title: Text("Menu"),
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Theme.of(context).primaryColor,
          type: BottomNavigationBarType.fixed,
          //backgroundColor: Color(0xFF6200EE),
          selectedItemColor: Colors.blue[800],
          unselectedItemColor: Colors.indigo.withOpacity(.60),
          selectedFontSize: 14,
          unselectedFontSize: 14,
          currentIndex:
              _selectedIndex, // this will be set when a new tab is tapped
          onTap: (value) {
            _onItemTapped(value); // Respon to item press.
          },
          items: [
            BottomNavigationBarItem(
              title: Text('Principal'),
              icon: Icon(Icons.home),
            ),
            BottomNavigationBarItem(
              title: Text('Ordenes'),
              icon: Icon(Icons.linear_scale),
            ),
            BottomNavigationBarItem(
              title: Text('Car'),
              icon: new Stack(
                children: <Widget>[
                  new IconButton(
                    icon: new Icon(
                      Icons.shopping_cart,
                      color: Colors.blue[800],
                    ),
                    onPressed: null,
                  ),
                  new Positioned(
                      child: new Stack(
                    children: <Widget>[
                      new Icon(Icons.brightness_1,
                          size: 22.0, color: Colors.red[700]),
                      new Positioned(
                          top: 4.0,
                          right: 5,
                          child: new Center(
                            child: StreamBuilder<int>(
                                stream: ordenBloc.getNumberCar,
                                initialData: ordenBloc.getNumberCarCurrent,
                                builder: (BuildContext context,
                                    AsyncSnapshot<int> snapshot) {
                                  return Text(
                                    '${snapshot.data}',
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.w500),
                                  );
                                }),
                          )),
                    ],
                  )),
                ],
              ),
              //Icon(Icons.shopping_cart),
            ),
            BottomNavigationBarItem(
              title: Text('Mis Sitios'),
              icon: Icon(Icons.add_location),
            ),
          ],
        ),
        body: _children[_selectedIndex],
      ),
    );
  }
}

class MenuLateral extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Padding(
              padding: EdgeInsets.only(top: 90, left: 0),
              child: Text(
                "Bienvenido",
                style: new TextStyle(
                    color: Colors.deepOrange[900],
                    fontSize: 40.0,
                    fontWeight: FontWeight.w500),
              ),
            ),
            decoration: BoxDecoration(
                color: Colors.green,
                image: DecorationImage(
                    fit: BoxFit.fitWidth,
                    image: AssetImage('assets/image/fodo_logo.png'))),
          ),
          ListTile(
            leading: Icon(
              Icons.verified_user,
              color: Colors.deepOrange[900],
            ),
            title: Text('Perfil'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(
              Icons.settings,
              color: Colors.deepOrange[900],
            ),
            title: Text('Preguntas Frecuentes'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(
              Icons.border_color,
              color: Colors.deepOrange[900],
            ),
            title: Text('Contáctanos'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(
              Icons.exit_to_app,
              color: Colors.deepOrange[900],
            ),
            title: Text('Salir'),
            onTap: () => {Navigator.of(context).pop()},
          ),
        ],
      ),
    );
  }
}
