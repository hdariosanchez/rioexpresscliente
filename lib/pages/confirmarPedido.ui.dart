import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rioexpresscliente/adicional/geoLocalization.dart';
import 'package:rioexpresscliente/bloc/carrito.bloc.dart';
import 'package:rioexpresscliente/bloc/direccion.bloc.dart';
import 'package:rioexpresscliente/models/direccion.model.dart';

import 'package:rioexpresscliente/models/userLocation.model.dart';

import 'package:image_picker/image_picker.dart';
import 'package:rioexpresscliente/pages/addMiDireccion.ui.dart';
import 'package:rioexpresscliente/utils/bubble_indication_painter.dart';
import 'package:rioexpresscliente/pages/menuNegocios.ui.dart';

import 'package:toast/toast.dart';

import 'menuPrincipal.ui.dart';

/*COMPONENTES DE ANIMACION*/
final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class ConfirmarPedido extends StatefulWidget {
  @override
  _ConfirmarPedidoState createState() => _ConfirmarPedidoState();
}

class _ConfirmarPedidoState extends State<ConfirmarPedido> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  File _image;
  final picker = ImagePicker();
  bool _isSaveFactura = false;

  @override
  void dispose() {
    super.dispose();
  }

  bool loaderFlag = false;

  String numeroVivenEnVivienda;

  String selectedDireccionFin;
  final _formKey = GlobalKey<FormState>();

  TextEditingController _controllerRucCedula = new TextEditingController();
  TextEditingController _controllerNombreApellido = new TextEditingController();
  TextEditingController _controllerCelular = new TextEditingController();
  TextEditingController _controllerCelularAdicional =
      new TextEditingController();
  TextEditingController _controllerDireccionEntrega =
      new TextEditingController();
  TextEditingController _controllerObservaciones = new TextEditingController();

  //bool _isLoading = false;

  void _upload() {
    if (_image == null) return;
    String base64Image = base64Encode(_image.readAsBytesSync());
    String fileName = _image.path.split("/").last;
  }

  Future getImage(int opcion) async {
    //
    switch (opcion) {
      case 1:
        {
          final pickedFile = await picker.getImage(source: ImageSource.gallery);
          if (pickedFile != null) {
            setState(() {
              _image = File(pickedFile.path);
            });
          }
        }
        break;
      case 2:
        {
          final pickedFile = await picker.getImage(source: ImageSource.camera);
          if (pickedFile != null) {
            setState(() {
              _image = File(pickedFile.path);
            });
          }
        }
        break;
    }

    bool switchControl = false;
    var textHolder = 'Switch is OFF';

    void toggleSwitch() {
      if (switchControl == false) {
        setState(() {
          switchControl = true;
          textHolder = 'Switch is ON';
        });
        print('Switch is ON');
        // Put your code here which you want to execute on Switch ON event.

      } else {
        setState(() {
          switchControl = false;
          textHolder = 'Switch is OFF';
        });
        print('Switch is OFF');
        // Put your code here which you want to execute on Switch OFF event.
      }
    }

    /*
    PermissionStatus permissionResult =
        await SimplePermissions.requestPermission(
            Permission.WriteExternalStorage);
    if (permissionResult == PermissionStatus.authorized) {
      // code of read or write file in external storage (SD card)
      final pickedFile = await picker.getImage(source: ImageSource.gallery);
      setState(() {
        _image = File(pickedFile.path);
      });
    } else {
      _showAlertDialogInfo();
    }
    */
  }

  void _showAlertDialogInfo() {
    showDialog(
      context: context,
      builder: (_) => Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)), //this right here
        child: Container(
          height: 190,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset(
                    'assets/image/icon-camera-foto.png',
                    width: 50.0,
                  ),
                ),
                Text(
                  "No ha concedido permisos para obtener una imagen",
                  style: new TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            //getImage();
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancelar",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFFe27468),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Reintentar",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFF1BC0C5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showAlertDialog() {
    showDialog(
      context: context,
      builder: (_) => Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)), //this right here
        child: Container(
          height: 190,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset(
                    'assets/image/icon-camera-foto.png',
                    width: 50.0,
                  ),
                ),
                Text(
                  "Desea agregar una imagen desde Galeria o tomar una fotografia",
                  style: new TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            getImage(2);
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Camara",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFFe27468),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            getImage(1);
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Galleria",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFF1BC0C5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  var listadoMisDirecciones;

  @override
  void initState() {
    super.initState();
    direccionBloc.paginaOrdenRapida(true);
    direccionBloc.fetchAllMyDirectionsForDropDow2();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Image.network(
          "https://www.todoiphone.net/wp-content/uploads/2014/03/WhatsApp-Wallpaper-28.png",
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.cover,
        ),
        Scaffold(
          key: _scaffoldState,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            title: Text("Confirmar Pedido"),
          ),
          body: SingleChildScrollView(
            child: Container(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Image.asset(
                        'assets/image/delivery_fast.png',
                        width: 60.0,
                      ),
                    ),
                    new TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.account_balance_wallet),
                        hintText: 'Ingrese cédula o Ruc',
                        labelText: 'RUC O CÉDULA',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Por favor ingrese un numero válido';
                        }
                      },
                      keyboardType: TextInputType.number,
                      controller: _controllerRucCedula,
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.person),
                        hintText: 'Ingrese nombre y apellido',
                        labelText: 'NOMBRES Y APELLIDOS',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Por favor ingrese nombres válidos';
                        }
                      },
                      controller: _controllerNombreApellido,
                    ),
                    new TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.phone_android),
                        hintText: 'Ingrese su número de celular',
                        labelText: 'CELULAR',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Por favor ingrese celular válido';
                        }
                      },
                      controller: _controllerCelular,
                      keyboardType: TextInputType.phone,
                    ),
                    new TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.speaker_phone),
                        hintText: 'Ingrese numero de familiar o amigo',
                        labelText: '\(OPCIONAL\) CELULAR ADICIONAL',
                      ),
                      /*
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Por favor ingrese celular válido';
                        }
                        
                      },
                      */
                      //keyboardType: TextInputType.emailAddress,
                      keyboardType: TextInputType.phone,
                      controller: _controllerCelularAdicional,
                    ),
                    new TextFormField(
                      maxLines: 3,
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.swap_horizontal_circle),
                        hintText:
                            'Ej. Casa de 1 planta, color marron en la Av Leopoldro Freire y Calle Atenas, esquina',
                        labelText: 'DIRECCION DE ENTREGA',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Por favor ingrese datos validos';
                        }
                      },

                      //keyboardType: TextInputType.emailAddress,
                      keyboardType: TextInputType.text,
                      controller: _controllerDireccionEntrega,
                    ),
                    new TextFormField(
                      maxLines: 4,
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.add_comment),
                        hintText:
                            'Ej. Deseo que me llamen antes de realizar la compra para confirmar / Deseo cambiar sabor de bebida por Sabor Negra',
                        labelText: '\(OPCIONAL\) OBSERVACIONES',
                      ),
                      /*
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Por favor ingrese datos válido';
                        }
                      },
                      */
                      //keyboardType: TextInputType.emailAddress,
                      keyboardType: TextInputType.text,
                      controller: _controllerObservaciones,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              gradient: new LinearGradient(
                                  colors: [
                                    Colors.black12,
                                    Colors.black,
                                  ],
                                  begin: const FractionalOffset(0.0, 0.0),
                                  end: const FractionalOffset(1.0, 1.0),
                                  stops: [0.0, 1.0],
                                  tileMode: TileMode.clamp),
                            ),
                            width: 100.0,
                            height: 1.0,
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 15.0, top: 15.0, right: 15.0),
                            child: Text(
                              "Ubicación",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16.0,
                                  fontFamily: "WorkSansMedium"),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              gradient: new LinearGradient(
                                  colors: [
                                    Colors.black,
                                    Colors.black12,
                                  ],
                                  begin: const FractionalOffset(0.0, 0.0),
                                  end: const FractionalOffset(1.0, 1.0),
                                  stops: [0.0, 1.0],
                                  tileMode: TileMode.clamp),
                            ),
                            width: 100.0,
                            height: 1.0,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 12.0, top: 0, right: 12.0, bottom: 0.0),
                      child: Text(
                        "Puedes ingresar la Ubicación en el mapa o seleccionar una de tus ubicaciones registradas previamente",
                        style: TextStyle(
                          color: Colors.black54,
                          fontSize: 14.0,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 12.0, top: 0.0, right: 12.0, bottom: 0.0),
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: 75,
                            height: 75,
                            child: Image.asset('assets/image/googlemap.png'),
                          ),
                          new FlatButton.icon(
                            //color: Colors.cyanAccent[80],
                            textColor: Colors.black,
                            splashColor: Colors.blueAccent,
                            icon: const Icon(
                              Icons.radio_button_checked,
                              size: 18.0,
                              color: Colors.blueAccent,
                            ),
                            label: const Text(
                              'Opcional (Ubicación en mapa)',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                                decorationStyle: TextDecorationStyle.solid,
                              ),
                            ),
                            onPressed: () async {
                              if (direccionBloc
                                      .getCurrentPositionEntregaOrden !=
                                  null) {
                                direccionBloc.paginaOrdenRapida(true);
                              }
                              setState(() {
                                selectedDireccionFin = null;
                              });
                              //Navigator.pushNamed(context, '/addMiDireccion');
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (ctxt) => AddMiDireccion(),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        StreamBuilder<LatLng>(
                            stream: direccionBloc.positionEntregaOrden,
                            initialData: null,
                            builder: (BuildContext context,
                                AsyncSnapshot<LatLng> snapshot) {
                              return Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(0),
                                    decoration: snapshot.data == null
                                        ? BoxDecoration(
                                            color: Colors.red,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          )
                                        : BoxDecoration(
                                            color: Colors.green,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                    constraints: BoxConstraints(
                                      minWidth: 18,
                                      minHeight: 18,
                                    ),
                                    child: snapshot.data == null
                                        ? Icon(
                                            Icons.error_outline,
                                            size: 16.0,
                                            color: Colors.white,
                                          )
                                        : Icon(
                                            Icons.check_circle_outline,
                                            size: 16.0,
                                            color: Colors.white,
                                          ),
                                  ),
                                  snapshot.data == null
                                      ? Text("Sin Coordenada de entrega")
                                      : Text("Coordenadas ingresadas"),
                                ],
                              );
                            }),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 12.0, top: 12.0, right: 12.0, bottom: 0.0),
                      child: Text(
                        "MIS UBICACIONES",
                        style: TextStyle(
                          //color: Colors.black38,
                          fontSize: 15.0,
                          fontWeight: FontWeight.normal,
                        ),
                        textAlign: TextAlign.justify,
                        textDirection: TextDirection.rtl,
                      ),
                    ),
                    StreamBuilder<MiDireccionModelList>(
                      stream: direccionBloc.allMyDirections,
                      initialData: null,
                      builder: (BuildContext context,
                          AsyncSnapshot<MiDireccionModelList> snapshot) {
                        if (!snapshot.hasData)
                          return CircularProgressIndicator();
                        return Padding(
                          padding: EdgeInsets.only(
                              left: 12.0, top: 0.0, right: 12.0, bottom: 0.0),
                          child: DropdownButton<String>(
                            hint: new Text("\(Opcional\) Seleccione Ubicación"),
                            value: selectedDireccionFin,
                            icon: Icon(Icons.arrow_downward),
                            isDense: true,
                            iconSize: 24,
                            elevation: 16,
                            isExpanded: true,
                            style: TextStyle(color: Colors.deepPurple),
                            underline: Container(
                              height: 3,
                              color: Colors.deepPurpleAccent,
                            ),
                            items: snapshot.data.results
                                .map((miDireccion) => DropdownMenuItem<String>(
                                      child: Text(miDireccion.nombre),
                                      value:
                                          "${miDireccion.latitud},${miDireccion.longitud}",
                                    ))
                                .toList(),
                            onChanged: (String value) {
                              List<String> position = value.split(",");
                              //double lat = double.parse(position[0].trim());
                              //double lng = double.parse(position[1].trim());
                              direccionBloc.currentPositionParseDouble(value);
                              direccionBloc.currentPositionInicioFin();
                              setState(() {
                                selectedDireccionFin = value;
                              });

                              print(
                                  "viendo que direccion escojo*************************> ${position[0].trim()},${position[1].trim()}");
                            },
                          ),
                        );
                      },
                    ),
                    _isSaveFactura ? CircularProgressIndicator() : Container(),
                    /*
                    Padding(
                      padding: EdgeInsets.only(
                          left: 12.0, top: 20.0, right: 12.0, bottom: 0.0),
                      child:
                          Text("AGREGUE UNA IMAGEN DE REFERENCIA A SU VIVIENDA",
                              style: TextStyle(
                                //color: Colors.black38,
                                fontSize: 15.0,
                                fontWeight: FontWeight.normal,
                              ),
                              textAlign: TextAlign.justify),
                    ),
                    
                    Center(
                      child: _image == null
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.all(1),
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  constraints: BoxConstraints(
                                    minWidth: 18,
                                    minHeight: 18,
                                  ),
                                  child: Icon(
                                    Icons.error_outline,
                                    size: 16.0,
                                    color: Colors.white,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "No tiene ninguna imagen agregada",
                                    style: new TextStyle(
                                        color: Colors.black,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ],
                            )
                          : Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0)),
                                color: const Color(0xFF1BC0C5),
                              ),
                              child: Image.file(
                                _image,
                                height:
                                    MediaQuery.of(context).size.height * 0.38,
                                width: MediaQuery.of(context).size.width * 0.9,
                              ),
                            ),
                    ),
                    */
                    Center(
                      child: RaisedButton(
                        onPressed: () async {
                          print("------------->---->--> $selectedDireccionFin");
                          print(
                              "------------->---->-->--> ${direccionBloc.getCurrentPositionEntregaOrden}");
                          if (_formKey.currentState.validate() &&
                              (selectedDireccionFin != null ||
                                  direccionBloc
                                          .getCurrentPositionEntregaOrden !=
                                      null)) {
                            var position =
                                direccionBloc.getCurrentPositionEntregaOrden;
                            //setState(() => _isLoading = true);
                            String rucCedula =
                                _controllerRucCedula.text.toString();
                            String nombreApellido =
                                _controllerNombreApellido.text.toString();
                            String celular = _controllerCelular.text.toString();
                            String celularAdicional =
                                _controllerCelularAdicional.text.toString();
                            String direccionEntrega =
                                _controllerDireccionEntrega.text.toString();
                            String observaciones =
                                _controllerObservaciones.text.toString();
                            double latitud = position.latitude;
                            double longitud = position.longitude;
                            setState(() => _isSaveFactura = true);
                            ordenBloc.saveDatosFactura(
                                rucCedula,
                                nombreApellido,
                                celular,
                                celularAdicional,
                                direccionEntrega,
                                observaciones,
                                latitud,
                                longitud);
                            ordenBloc.createFacturaBool.listen(
                              (event) {
                                if (event) {
                                  //direccionBloc.fetchAllMyDirections();
                                  _scaffoldState.currentState.showSnackBar(
                                    SnackBar(
                                      content: Text(
                                          "Ingreso Correcto de su Factura"),
                                    ),
                                  );
                                  Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              MenuPrincipal()),
                                      (Route<dynamic> route) => false);
                                } else {
                                  _scaffoldState.currentState.showSnackBar(
                                    SnackBar(
                                      content: Text("Ingreso de factura fallo"),
                                    ),
                                  );
                                }
                              },
                            );
                          } else {
                            _scaffoldState.currentState.showSnackBar(
                              SnackBar(
                                content: Text("Por favor rellene los campos"),
                              ),
                            );
                            return;
                          }
                        },
                        child: Text(
                          "ORDENAR PEDIDO".toUpperCase(),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        color: Color(0xFFD81B60),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => {
              //_showAlertDialog()
              //getImage()
            },
            tooltip: 'Pick Image',
            child: Icon(Icons.add_a_photo),
          ),
        ),
      ],
    );
  }

  void showInSnackBar(String value) {
    FocusScope.of(context).requestFocus(new FocusNode());
    _scaffoldKey.currentState?.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        textAlign: TextAlign.center,
        style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            fontFamily: "WorkSansSemiBold"),
      ),
      backgroundColor: Colors.blue,
      duration: Duration(seconds: 3),
    ));
  }

//********************************************************************** */¨
//*************************************************** */

  Future getLocation() async {
    setState(() {
      loaderFlag = true;
    });
    UserLocation userLocation = await LocationService().getLocation();
    Toast.show(
        "Toast plugin app -->" + userLocation.longitude.toString(), context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    setState(() {
      loaderFlag = false;
    });
  }

  // user defined function
  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert Dialog title"),
          content: new Text("Alert Dialog body"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
