import 'dart:math';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:rioexpresscliente/bloc/carrito.bloc.dart';
//import 'dart:developer';
import 'package:rioexpresscliente/components/itemEspecificacion/itemPrecio.component.dart';
import 'package:rioexpresscliente/components/itemEspecificacion/itemTitle.component.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:rioexpresscliente/configuration/local.dart';

//IMPORTANDO MODELO DE NEGOCIOS
import 'package:rioexpresscliente/models/negocio.model.dart';
import 'package:rioexpresscliente/models/producto.model.dart';
//IMPORTANDO PATRON BLOC DEL NEGOCIO
import 'package:rioexpresscliente/bloc/productoNegocio.bloc.dart';

//IMPORTANDO COMPONENTE DE GEOLOCATOR
import 'package:geolocator/geolocator.dart';
import 'package:rioexpresscliente/pages/carritoCompra.ui.dart';

class NegocioProductosItems extends StatefulWidget {
  NegocioProductosItems({Key key}) : super(key: key);
  //final NegocioModel negocioWithServices;

  @override
  _NegocioProductosItemsState createState() => _NegocioProductosItemsState();
}

class _NegocioProductosItemsState extends State<NegocioProductosItems> {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  //static IpConfig local = new IpConfig("35.247.235.167", "5000");
  static IpConfig local = new IpConfig("192.168.1.15", "5000");
  bool banderaCarrito = false;
/*
  void showInSnackBar(String value) {
    FocusScope.of(context).requestFocus(new FocusNode());
    _scaffoldKey.currentState?.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        textAlign: TextAlign.center,
        style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            fontFamily: "WorkSansSemiBold"),
      ),
      backgroundColor: Colors.blue,
      duration: Duration(seconds: 3),
    ));
  }
  */
  void _showAlertDialog(String titulo) {
    showDialog(
      context: context,
      builder: (_) => Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)), //this right here
        child: Container(
          height: 190,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset(
                    'assets/image/ico_alien.png',
                    width: 50.0,
                  ),
                ),
                Text(
                  titulo,
                  style: new TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Ok",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFFe27468),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //define method to call
  void showInSnackBar(String value) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  @override
  void initState() {
    super.initState();
    productoBloc.fetchAllProductoByNegocio("");
    //ordenBloc.fetchAllOrdenProduct(null);
    //var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);
  }

/*
  @override
  void dispose() {
    //negocioBloc.dispose();
    super.dispose();
  }

  */

  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Lista de Productos"),
        actions: <Widget>[
          /*
          new IconButton(
            icon: new Icon(
              Icons.label,
              color: Colors.blue[800],
            ),
            onPressed: () {},
          ),
          new Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Container(
                height: 150.0,
                width: 30.0,
                child: new GestureDetector(
                  onTap: () {
                    //productoBloc.fetchAllProductoCar();
                    //Navigator.pushNamed(context, '/carritoCompra');
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CarritoCompra(),
                      ),
                    );
                  },
                  child: new Stack(
                    children: <Widget>[
                      new IconButton(
                        icon: new Icon(
                          Icons.shopping_cart,
                          color: Colors.blue[800],
                        ),
                        onPressed: null,
                      ),
                      new Positioned(
                          child: new Stack(
                        children: <Widget>[
                          new Icon(Icons.brightness_1,
                              size: 22.0, color: Colors.red[700]),
                          new Positioned(
                              top: 4.0,
                              right: 5,
                              child: new Center(
                                child: StreamBuilder<int>(
                                    stream: ordenBloc.getNumberCar,
                                    initialData: ordenBloc.getNumberCarCurrent,
                                    builder: (BuildContext context,
                                        AsyncSnapshot<int> snapshot) {
                                      return Text(
                                        '${snapshot.data}',
                                        style: new TextStyle(
                                            color: Colors.white,
                                            fontSize: 12.0,
                                            fontWeight: FontWeight.w500),
                                      );
                                    }),
                              )),
                        ],
                      )),
                    ],
                  ),
                )),
          )
          */
        ],
      ),
      //body: NegocioProductosItemsWidget(),
      body: StreamBuilder(
        stream: productoBloc.allProductos,
        builder: (context, AsyncSnapshot<ProductoModelList> snapshot) {
          if (snapshot.hasData) {
            productoBloc.allProductoTipoNegocio.servicioNegocio.forEach(
              (servicioItem) {
                servicioItem.results
                    .removeRange(0, servicioItem.results.length);
                for (var itemProducto in snapshot.data.results)
                  if (servicioItem.id == itemProducto.idCategoriaServicio) {
                    servicioItem.results.add(itemProducto);
                  }
              },
            );

            return new CustomScrollView(
              slivers:
                  _buildSlivers(context, productoBloc.allProductoTipoNegocio),
            );
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  List<Widget> _buildSlivers(
      BuildContext context, NegocioModel negocioWithServices) {
    List<Widget> slivers = new List<Widget>();
    int i = 0;
    slivers.addAll(_buildHeaderBuilderLists(context, i, negocioWithServices));
    return slivers;
  }

  List<Widget> _buildHeaderBuilderLists(
      BuildContext context, int firstIndex, NegocioModel negocioWithServices) {
    return List.generate(
      negocioWithServices.servicioNegocio.length,
      (sliverIndex) {
        sliverIndex += firstIndex;
        //numeroProductosServicio();
        return new SliverStickyHeaderBuilder(
          builder: (context, state) => _buildAnimatedHeader(
              context, sliverIndex, state, negocioWithServices),
          sliver: new SliverList(
            delegate: new SliverChildBuilderDelegate(
              (context, i) => Card(
                child: Column(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        negocioWithServices.servicioNegocio[sliverIndex]
                                    .results[i].urlImagen !=
                                null
                            ? Image.network(
                                'http://${local.getIp()}:${local.getPort()}/imagenProducto/${negocioWithServices.servicioNegocio[sliverIndex].results[i].id}${negocioWithServices.servicioNegocio[sliverIndex].results[i].urlImagen}.jpg',
                                fit: BoxFit.fill,
                              )
                            : CircularProgressIndicator(),
                        Positioned(
                          top: 5,
                          left: 20,
                          child: Row(
                            children: <Widget>[
                              SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width * .85),
                              Icon(Icons.more_vert,
                                  size: 25, color: Colors.white),
                            ],
                          ),
                        ),
                        new Positioned(
                          bottom: 70.0,
                          left: 10.0,
                          right: 10.0,
                          child: Card(
                            color: Colors.transparent,
                            elevation: 0.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(1.0),
                                  child: TitleEspecification(
                                      1,
                                      negocioWithServices
                                          .servicioNegocio[sliverIndex]
                                          .results[i]
                                          .nombreCorto),
                                ),
                              ],
                            ),
                          ),
                        ),
                        new Positioned(
                          top: 10.0,
                          left: 10.0,
                          child: ItemPrecio(
                              false,
                              "\$ ${negocioWithServices.servicioNegocio[sliverIndex].results[i].precio}",
                              20),
                        ),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        negocioWithServices.servicioNegocio[sliverIndex]
                            .results[i].descripcionProducto,
                        style: const TextStyle(
                          fontWeight: FontWeight.w300,
                          //color: Colors.white,
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.justify,
                        //textDirection: TextDirection.rtl,
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new FlatButton.icon(
                            // Un icono puede recibir muchos atributos, aqui solo usaremos icono, tamaño y color
                            icon: const Icon(
                              Icons.add_shopping_cart,
                              size: 25.0,
                              color: Colors.white,
                            ),
                            label: const Text(
                              'Añadir',
                              style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 17,
                              ),
                            ),
                            color: Colors.pinkAccent,
                            // Esto mostrara 'Me encanta' por la terminal
                            onPressed: () {
                              print("enviamos el producto");
                              ordenBloc.createOrden(
                                  negocioWithServices
                                      .servicioNegocio[sliverIndex].results[i],
                                  productoBloc.allProductoTipoNegocio.id);
                              ordenBloc.getNewOrdenCreate.listen(
                                (event) {
                                  print("Ingreso la orden o no--> $event");

                                  event
                                      ? _showAlertDialog(
                                          "Ingreso correctamente")
                                      :
                                      /*
                                      Exception has occurred.
                                      FlutterError (Looking up a deactivated widget's ancestor is unsafe.
                                      At this point the state of the widget's element tree is no longer stable.
                                      To safely refer to a widget's ancestor in its dispose() method, save a reference to the ancestor by calling
                                      dependOnInheritedWidgetOfExactType() in the widget's didChangeDependencies() method.)
                                      */
                                      _showAlertDialog(
                                          "No puede agregar a la misma orden");
                                },
                              );
                              banderaCarrito = true;
                              print("presione con -_> $sliverIndex");
                            },
                          ),
                        ),
                        //SizedBox(width: 10),
                        //Icon(Icons.more_vert, size: 32, color: Colors.white),
                      ],
                    ),
                  ],
                ),
              ),
              //childCount: numeroProductosServicio(),
              childCount: negocioWithServices
                  .servicioNegocio[sliverIndex].results.length,
            ),
          ),
        );
      },
    );
  }

  Widget _buildAnimatedHeader(BuildContext context, int index,
      SliverStickyHeaderState state, NegocioModel negocioWithServices) {
    return GestureDetector(
      onTap: () => Scaffold.of(context).showSnackBar(
        new SnackBar(
          content: Text('$index'),
        ),
      ),
      child: new Container(
        height: 40.0,
        color: (state.isPinned ? Colors.blue : Colors.blue)
            .withOpacity(1.0 - state.scrollPercentage),
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        alignment: Alignment.centerLeft,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          //crossAxisAlignment: CrossAxisAlignment.start,
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(1.0),
              child: Icon(
                Icons.adb,
                color: Colors.white,
                size: 25.0,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(1.0),
              child: Text(
                negocioWithServices.servicioNegocio[index].nombre,
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
