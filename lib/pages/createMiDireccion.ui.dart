import 'package:flutter/material.dart';

import 'package:rioexpresscliente/bloc/direccion.bloc.dart';
import 'package:rioexpresscliente/components/inputRegister/inputLoginAndRegister.dart';
import 'package:rioexpresscliente/models/direccion.model.dart';
import 'package:rioexpresscliente/pages/direccionList.ui.dart';
import 'package:rioexpresscliente/pages/menuPrincipal.ui.dart';
import 'package:rioexpresscliente/pages/principal.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:toast/toast.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class CreateMiDireccion extends StatefulWidget {
  @override
  CreateMiDireccionState createState() => CreateMiDireccionState();
}

class CreateMiDireccionState extends State<CreateMiDireccion> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _controllerTitle = TextEditingController();
  bool _isLoading = false;
  var idUser;
  SharedPreferences sharedPreferences;

  idClienteFuncion() async {
    sharedPreferences = await SharedPreferences.getInstance();
    idUser = sharedPreferences.getString("idUser");
  }

  @override
  void initState() {
    direccionBloc.currentPositionLatLng.listen((event) {
      print(
          "viendo datos desde el init de create lat --> ${event.latitude} Y lat --> ${event.longitude}");
    });
    super.initState();
    idClienteFuncion();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Image.network(
          "https://www.todoiphone.net/wp-content/uploads/2014/03/WhatsApp-Wallpaper-28.png",
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.cover,
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            title: Text("Registro Nueva Dirección"),
          ),
          body: SingleChildScrollView(
            child: Container(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                      width: 10,
                    ),
                    Image.network(
                      'https://img.icons8.com/cotton/2x/worldwide-location.png',
                      width: 60.0,
                    ),
                    InputLoginAndRegister(
                        2,
                        3,
                        'NOMBRE DE SU UBICACIÓN',
                        'Nombre lugar Ej. Casa mis padres',
                        'Por favor ingrese nombre válido',
                        _controllerTitle),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: RaisedButton(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            // Si el formulario es válido, queremos mostrar un Snackbar
                            setState(() => _isLoading = true);
                            String userId = idUser;
                            String title = _controllerTitle.text.toString();
                            double latitude = direccionBloc
                                .currentPositionLatLngSimple.latitude;
                            double longitude = direccionBloc
                                .currentPositionLatLngSimple.longitude;

                            MiDireccion direccion = new MiDireccion();
                            direccion.id = "";
                            direccion.idCliente = userId;
                            direccion.nombre = title;
                            direccion.longitud = longitude;
                            direccion.latitud = latitude;

                            direccionBloc.createDireccion(direccion);
                            direccionBloc.createDirectionBool.listen(
                              (event) {
                                setState(() => _isLoading = false);
                                if (event) {
                                  direccionBloc.fetchAllMyDirections();
                                  //Navigator.pop(context);

                                  Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              Principal(title: "Principal")),
                                      (Route<dynamic> route) => false);

                                  /*
                                  Navigator.of(context).popUntil(
                                    ModalRoute.withName('/miDireccion'),
                                  );
                                */
                                  /*
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (ctxt) => MiDireccionP(),
                                    ),
                                  );
                                  */
                                } else {
                                  _scaffoldState.currentState.showSnackBar(
                                    SnackBar(
                                      content: Text("Envio de datos Fallo"),
                                    ),
                                  );
                                }
                              },
                            );
                          } else {
                            _scaffoldState.currentState.showSnackBar(
                              SnackBar(
                                content: Text("Por favor rellene los campos"),
                              ),
                            );
                            return;
                          }
                        },
                        child: Text(
                          "REGISTRAR".toUpperCase(),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        color: Color(0xFFD81B60),
                      ),
                    ),
                    _isLoading ? CircularProgressIndicator() : Container(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
