import 'dart:convert';

import "package:flutter/material.dart";
import 'package:flutter_socket_io/flutter_socket_io.dart';
import 'package:flutter_socket_io/socket_io_manager.dart';

//import 'package:rioexpress_repartidor/const/_const.dart';

class SeguimientoOrden extends StatefulWidget {
  SeguimientoOrden({Key key}) : super(key: key);

  @override
  _StepperState createState() => _StepperState();
}

class _StepperState extends State<SeguimientoOrden> {
  int currentStep = 0;
  bool complete = false;
  StepperType stepperType = StepperType.vertical;

  SocketIO socketIO;
  final _baseUrl = "http://192.168.1.15:5000";

  void _showAlertDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)), //this right here
        child: Container(
          height: 190,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset(
                    'assets/image/icon_success.png',
                    width: 50.0,
                  ),
                ),
                Text(
                  "¿Desea dar por concluida la entrega de su pedido?",
                  style: new TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            setState(() => complete = false);
                            Navigator.pop(context);
                          },
                          child: Text(
                            "NO",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFFe27468),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            setState(() => complete = false);
                            //Navigator.pop(context);
                          },
                          child: Text(
                            "Si",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFF1BC0C5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    //Creating the socket
    socketIO = SocketIOManager().createSocketIO(
      _baseUrl,
      '/',
    );
    //Call init before doing anything with socket
    socketIO.init();
    //Subscribe to an event to listen to
    socketIO.subscribe('receive_message', (jsonData) {
      //Convert the JSON data received into a Map
      Map<String, dynamic> data = json.decode(jsonData);
      print("viendo que regresa en tiempo real----->>> ${data['message']}");

      /*
      this.setState(() => messages.add(data['message']));
      scrollController.animateTo(
        scrollController.position.maxScrollExtent,
        duration: Duration(milliseconds: 600),
        curve: Curves.ease,
      );
      */
    });

    socketIO.subscribe('typing', (jsonData) {
      print("Esta escribiendo...");
    });

    //Connect to the socket
    socketIO.connect();
    super.initState();
  }

  llamarCliente() {
    print("llamando al cliente");
  }

  next() {
    socketIO.sendMessage('send_message',
        json.encode({'message': "Enviado desde repartidor Cliente!!!!"}));
    if (currentStep + 1 != steps.length) {
      goTo(currentStep + 1);
    } else {
      setState(() {
        complete = true;
      });
      //_showAlertDialog();
    }
  }

  cancel() {
    if (currentStep > 0) {
      goTo(currentStep - 1);
    }
  }

  goTo(int step) {
    setState(() => currentStep = step);
  }

  List<Step> steps = [
    Step(
      title: const Text('Comprando el'),
      subtitle: const Text("Producto"),
      isActive: false,
      state: StepState.indexed,
      content: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          /*
          CircleAvatar(
            backgroundColor: Colors.green,
            child: Icon(Icons.check),
          ),
          */
          CircleAvatar(
            backgroundColor: Colors.red,
            child: Icon(Icons.cancel),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: Icon(Icons.attach_money, size: 40),
            ),
          ),
        ],
      ),
    ),
    Step(
      isActive: false,
      state: StepState.indexed,
      title: const Text('En Camino'),
      subtitle: const Text("a entregar"),
      content: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          /*
          CircleAvatar(
            backgroundColor: Colors.green,
            child: Icon(Icons.check),
          ),*/
          CircleAvatar(
            backgroundColor: Colors.red,
            child: Icon(Icons.cancel),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: Icon(Icons.motorcycle, size: 40),
            ),
          ),
        ],
      ),
    ),
    Step(
      isActive: false,
      state: StepState.indexed,
      title: const Text('Llegue al punto'),
      subtitle: const Text("de encuentro"),
      content: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircleAvatar(
            backgroundColor: Colors.green,
            child: Icon(Icons.check),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: Icon(Icons.location_on, size: 40),
            ),
          ),
        ],
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Seguimiento Orden'),
      ),
      body: Column(
        children: <Widget>[
          complete
              ?
              /*
              Expanded(
                  child: Center(
                    child: AlertDialog(
                      title: new Text("Profile Created"),
                      content: new Text(
                        "Tada!",
                      ),
                      actions: <Widget>[
                        new FlatButton(
                          child: new Text("Close"),
                          onPressed: () {
                            setState(() => complete = false);
                          },
                        ),
                      ],
                    ),
                  ),
                )
                */
              //Container()
              Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(10.0)), //this right here
                  child: Container(
                    height: 190,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: Image.asset(
                              'assets/image/icon_success.png',
                              width: 50.0,
                            ),
                          ),
                          Text(
                            "¿Desea dar por concluida la entrega de su pedido?",
                            style: new TextStyle(
                                color: Colors.black,
                                fontSize: 15.0,
                                fontWeight: FontWeight.w500),
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: RaisedButton(
                                    onPressed: () {
                                      setState(() => complete = false);
                                      //Navigator.pop(context);
                                    },
                                    child: Text(
                                      "NO",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    color: const Color(0xFFe27468),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: RaisedButton(
                                    onPressed: () {
                                      setState(() => complete = false);
                                      //Navigator.pop(context);
                                    },
                                    child: Text(
                                      "Si",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    color: const Color(0xFF1BC0C5),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : Expanded(
                  child: Stepper(
                    type: stepperType,
                    //steps: steps,
                    steps: [
                      Step(
                        title: const Text('Comprando el'),
                        subtitle: const Text("Producto"),
                        isActive: false,
                        state: StepState.indexed,
                        content: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                CircleAvatar(
                                  backgroundColor: Colors.red,
                                  child: Icon(Icons.clear),
                                ),
                                /*
                                CircleAvatar(
                                  backgroundColor: Colors.green,
                                  child: Icon(Icons.check),
                                ),
                                */
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CircleAvatar(
                                    backgroundColor: Colors.white,
                                    child: Icon(Icons.attach_money, size: 40),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Step(
                        isActive: false,
                        state: StepState.indexed,
                        title: const Text('En Camino'),
                        subtitle: const Text("a entregar"),
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              backgroundColor: Colors.red,
                              child: Icon(Icons.clear),
                            ),
                            /*
                                CircleAvatar(
                                  backgroundColor: Colors.green,
                                  child: Icon(Icons.check),
                                ),
                                */
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Icon(Icons.motorcycle, size: 40),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Step(
                        isActive: false,
                        state: StepState.indexed,
                        title: const Text('Llegue al punto'),
                        subtitle: const Text("de encuentro"),
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              backgroundColor: Colors.green,
                              child: Icon(Icons.check),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Icon(Icons.location_on, size: 40),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                    currentStep: currentStep,
                    onStepContinue: next,
                    onStepTapped: (step) => goTo(step),
                    onStepCancel: cancel,
                  ),
                ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.red,
        child: Icon(Icons.local_phone),
        onPressed: llamarCliente,
      ),
    );
  }
}
