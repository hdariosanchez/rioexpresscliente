import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:rioexpresscliente/bloc/carrito.bloc.dart';
import 'package:rioexpresscliente/bloc/negocio.bloc.dart';
import 'package:rioexpresscliente/pages/login_page.ui.dart';
import 'package:rioexpresscliente/pages/negocio.ui.dart';
import 'package:rioexpresscliente/pages/ordenRapida.ui.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'menuNegocios.ui.dart';

class Principal extends StatefulWidget {
  Principal({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _PrincipalState createState() => new _PrincipalState();
}

class _PrincipalState extends State<Principal> {
  List<String> imageLinks = [
    'https://homepages.cae.wisc.edu/~ece533/images/fruits.png',
    'https://homepages.cae.wisc.edu/~ece533/images/cat.png',
    'https://homepages.cae.wisc.edu/~ece533/images/peppers.png',
    'https://homepages.cae.wisc.edu/~ece533/images/tulips.png'
  ];

  @override
  void initState() {
    super.initState();
    ordenBloc.fetchAllOrdenProduct();
    //logout();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.transparent,
      drawer: MenuLateral(),
      appBar: AppBar(
        title: Text("Principal"),
        actions: <Widget>[
          new Stack(
            children: <Widget>[
              new IconButton(
                icon: new Icon(
                  Icons.shopping_cart,
                  color: Colors.blue[800],
                ),
                onPressed: () => Navigator.pushNamed(context, '/carritoCompra'),
              ),
              new Positioned(
                  child: new Stack(
                children: <Widget>[
                  new Icon(Icons.brightness_1,
                      size: 22.0, color: Colors.red[700]),
                  new Positioned(
                      top: 4.0,
                      right: 5,
                      child: new Center(
                        child: StreamBuilder<int>(
                            stream: ordenBloc.getNumberCar,
                            initialData: ordenBloc.getNumberCarCurrent,
                            builder: (BuildContext context,
                                AsyncSnapshot<int> snapshot) {
                              return Text(
                                '${snapshot.data}',
                                style: new TextStyle(
                                    color: Colors.white,
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.w500),
                              );
                            }),
                      )),
                ],
              )),
            ],
          ),
        ],
      ),
      body: Container(
        color: Colors.deepOrange[900],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            CarouselSlider(
              options: CarouselOptions(
                height: 150,
                aspectRatio: 2.0,
                viewportFraction: 0.8,
                initialPage: 0,
                enableInfiniteScroll: true,
                reverse: false,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 3),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                autoPlayCurve: Curves.fastOutSlowIn,
                enlargeCenterPage: true,
                scrollDirection: Axis.horizontal,
              ),
              items: imageLinks.map((imageLink) {
                return Builder(
                  builder: (BuildContext context) {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 1.0),
                      child: Image.network(
                        imageLink,
                        fit: BoxFit.cover,
                      ),
                    );
                  },
                );
              }).toList(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new Container(
                  child: new Column(
                    children: <Widget>[
                      ClipOval(
                        child: Material(
                          color: Colors.white, // button color
                          child: InkWell(
                            splashColor: Colors.red, // inkwell color
                            child: SizedBox(
                              width: 75,
                              height: 75,
                              child: Image.asset('assets/image/ico_pedido.png'),
                            ),
                            onTap: () {
                              //Navigator.pushNamed(context, '/OrdenRapida');
                              /*                
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (ctxt) => OrdenRapida(),
                                ),
                              );
                              */
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: new Text(
                          "Pedido",
                          style: new TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  child: new Column(
                    children: <Widget>[
                      ClipOval(
                        child: Material(
                          color: Colors.white, // button color
                          child: InkWell(
                            splashColor: Colors.red, // inkwell color
                            child: SizedBox(
                              width: 75,
                              height: 75,
                              child: Image.asset(
                                  'assets/image/ico_transporte.png'),
                            ),
                            onTap: () {
                              print("todo bien");
                              //Navigator.pushNamed(context, '/OrdenRapida');
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: new Text(
                          "Transporte",
                          style: new TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new Container(
                  child: new Column(
                    children: <Widget>[
                      ClipOval(
                        child: Material(
                          color: Colors.white, // button color
                          child: InkWell(
                            splashColor: Colors.red, // inkwell color
                            child: SizedBox(
                              width: 75,
                              height: 75,
                              child:
                                  Image.asset('assets/image/ico_comidas.png'),
                            ),
                            onTap: () {
                              negocioBloc.asignNegocioTipo("Restaurante");
                              //Navigator.pushNamed(context, '/negocios');
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          NegocioItems()));
                              /*
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NegocioItems(
                                    tipoNegocio: "Restaurante",
                                  ),
                                ),
                              );
                            */
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: new Text(
                          "Restaurante",
                          style: new TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  child: new Column(
                    children: <Widget>[
                      ClipOval(
                        child: Material(
                          color: Colors.white, // button color
                          child: InkWell(
                            splashColor: Colors.red, // inkwell color
                            child: SizedBox(
                              width: 75,
                              height: 75,
                              child:
                                  Image.asset('assets/image/ico_farmacia.png'),
                            ),
                            onTap: () {
                              negocioBloc.asignNegocioTipo("Farmacia");
                              Navigator.pushNamed(context, '/negocios');
                              /* 
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NegocioItems(
                                    tipoNegocio: "Farmacia",
                                  ),
                                ),
                              );
                              */
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: new Text(
                          "Farmacia",
                          style: new TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new Container(
                  child: new Column(
                    children: <Widget>[
                      ClipOval(
                        child: Material(
                          color: Colors.white, // button color
                          child: InkWell(
                            splashColor: Colors.red, // inkwell color
                            child: SizedBox(
                              width: 75,
                              height: 75,
                              child: Image.asset(
                                  'assets/image/ico_licores_bebidas.png'),
                            ),
                            onTap: () {
                              negocioBloc.asignNegocioTipo("Licoreria");
                              Navigator.pushNamed(context, '/negocios');
                              /** 
                               * 
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NegocioItems(
                                    tipoNegocio: "Licoreria",
                                  ),
                                ),
                              );
                              */
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: new Text(
                          "Licores",
                          style: new TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  child: new Column(
                    children: <Widget>[
                      ClipOval(
                        child: Material(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(50.0),
                            bottomLeft: Radius.circular(50.0),
                          ),
                          color: Colors.white, // button color
                          child: InkWell(
                            splashColor: Colors.red, // inkwell color
                            child: SizedBox(
                              width: 75,
                              height: 75,
                              child: Image.asset(
                                  'assets/image/ico_regalos_mas.png'),
                            ),
                            onTap: () {
                              negocioBloc.asignNegocioTipo("Regalos");
                              Navigator.pushNamed(context, '/negocios');
                              /**
                               
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NegocioItems(
                                    tipoNegocio: "Regalos",
                                  ),
                                ),
                              );
                               */
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: new Text(
                          "Regalos y mas",
                          style: new TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new Container(
                  child: new Column(
                    children: <Widget>[
                      ClipOval(
                        child: Material(
                          color: Colors.white, // button color
                          child: InkWell(
                            splashColor: Colors.red, // inkwell color
                            child: SizedBox(
                              width: 75,
                              height: 75,
                              child: Image.asset('assets/image/ico_market.png'),
                            ),
                            onTap: () {
                              negocioBloc.asignNegocioTipo("Market");
                              Navigator.pushNamed(context, '/negocios');
                              /**
                               * 
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NegocioItems(
                                    tipoNegocio: "Market",
                                  ),
                                ),
                              );
                               */
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: new Text(
                          "Market",
                          style: new TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  child: new Column(
                    children: <Widget>[
                      ClipOval(
                        child: Material(
                          color: Colors.white, // button color
                          child: InkWell(
                            splashColor: Colors.red, // inkwell color
                            child: SizedBox(
                              width: 75,
                              height: 75,
                              child: Image.asset(
                                  'assets/image/ico_panaderia_pasteleria.png'),
                            ),
                            onTap: () {
                              negocioBloc.asignNegocioTipo("Panaderia");
                              Navigator.pushNamed(context, '/negocios');

                              /** 
                              
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NegocioItems(
                                    tipoNegocio: "Panaderia",
                                  ),
                                ),
                              );
                               */
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: new Text(
                          "Panaderia y mas",
                          style: new TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new Container(
                  child: new Column(
                    children: <Widget>[
                      ClipOval(
                        child: Material(
                          color: Colors.white, // button color
                          child: InkWell(
                            splashColor: Colors.red, // inkwell color
                            child: SizedBox(
                              width: 75,
                              height: 75,
                              child:
                                  Image.asset('assets/image/ico_papeleria.png'),
                            ),
                            onTap: () {
                              negocioBloc.asignNegocioTipo("Papeleria");
                              Navigator.pushNamed(context, '/negocios');
                              /** 
                              
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NegocioItems(
                                    tipoNegocio: "Papeleria",
                                  ),
                                ),
                              );
                               */
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: new Text(
                          "Papeleria",
                          style: new TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  child: new Column(
                    children: <Widget>[
                      ClipOval(
                        child: Material(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(50.0),
                            bottomLeft: Radius.circular(50.0),
                          ),
                          color: Colors.white, // button color
                          child: InkWell(
                            splashColor: Colors.red, // inkwell color
                            child: SizedBox(
                              width: 75,
                              height: 75,
                              child: Image.asset(
                                  'assets/image/ico_tecnologia.png'),
                            ),
                            onTap: () {
                              negocioBloc.asignNegocioTipo("Tecnologia");
                              Navigator.pushNamed(context, '/negocios');
                              /** 
                              
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NegocioItems(
                                    tipoNegocio: "Tecnologia",
                                  ),
                                ),
                              );
                               */
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: new Text(
                          "Técnologia",
                          style: new TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class MenuLateral extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Padding(
              padding: EdgeInsets.only(top: 90, left: 0),
              child: Text(
                "Bienvenido",
                style: new TextStyle(
                    color: Colors.deepOrange[900],
                    fontSize: 40.0,
                    fontWeight: FontWeight.w500),
              ),
            ),
            decoration: BoxDecoration(
                color: Colors.green,
                image: DecorationImage(
                    fit: BoxFit.fitWidth,
                    image: AssetImage('assets/image/fodo_logo.png'))),
          ),
          ListTile(
            leading: Icon(
              Icons.verified_user,
              color: Colors.deepOrange[900],
            ),
            title: Text('Perfil'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(
              Icons.add_location,
              color: Colors.deepOrange[900],
            ),
            title: Text('Direcciones'),
            onTap: () => {Navigator.pushNamed(context, '/miDireccion')},
          ),
          ListTile(
            leading: Icon(
              Icons.add_location,
              color: Colors.deepOrange[900],
            ),
            title: Text('Mis Ordenes'),
            onTap: () => {Navigator.pushNamed(context, '/OrdenesList')},
          ),
          ListTile(
            leading: Icon(
              Icons.settings,
              color: Colors.deepOrange[900],
            ),
            title: Text('Preguntas Frecuentes'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(
              Icons.border_color,
              color: Colors.deepOrange[900],
            ),
            title: Text('Contáctanos'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(
              Icons.exit_to_app,
              color: Colors.deepOrange[900],
            ),
            title: Text('Salir'),
            onTap: () => {Navigator.of(context).pop()},
          ),
        ],
      ),
    );
  }
}
