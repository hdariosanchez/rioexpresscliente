import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rioexpresscliente/bloc/carrito.bloc.dart';
import 'package:rioexpresscliente/components/buttons/circle_button.dart';
import 'package:rioexpresscliente/components/inputRegister/inputLoginAndRegister.dart';
import 'package:rioexpresscliente/components/itemEspecificacion/itemPrecio.component.dart';
import 'package:rioexpresscliente/configuration/local.dart';
import 'package:rioexpresscliente/models/orden.model.dart';
import 'package:rioexpresscliente/pages/confirmarPedido.ui.dart';

class CarritoCompra extends StatefulWidget {
  @override
  _CarritoCompraState createState() => _CarritoCompraState();
}

class _CarritoCompraState extends State<CarritoCompra> {
  final _formKey = GlobalKey<FormState>();
  bool _isData = false;

  @override
  void initState() {
    super.initState();

    ordenBloc.fetchAllOrdenProduct();
    //_showAlertDialog();
    // simply use this
    ordenBloc.getNumberCarCurrent == -1
        ? Timer.run(() {
            showDialog(
              context: context,
              builder: (BuildContext context) => Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.circular(10.0)), //this right here
                child: Container(
                  height: 190,
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                          child: Image.asset(
                            'assets/image/icon-factura.png',
                            width: 50.0,
                          ),
                        ),
                        Text(
                          "Desea una Factura de esta Orden",
                          style: new TextStyle(
                              color: Colors.black,
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500),
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: RaisedButton(
                                  onPressed: () {
                                    ordenBloc.totalFuncion(false);
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    "NO",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  color: const Color(0xFFe27468),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: RaisedButton(
                                  onPressed: () {
                                    ordenBloc.totalFuncion(true);
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    "Si",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  color: const Color(0xFF1BC0C5),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          })
        : Container();
  }

  void _showAlertDialog(int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)), //this right here
        child: Container(
          height: 190,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset(
                    'assets/image/icon_delete.png',
                    width: 50.0,
                  ),
                ),
                Text(
                  "Desea eliminar este producto de su Carrito de Compra",
                  style: new TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            ordenBloc.deleteProductoLocal(index, false);
                            Navigator.pop(context);
                          },
                          child: Text(
                            "NO",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFFe27468),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            ordenBloc.deleteProductoLocal(index, true);
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Si",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFF1BC0C5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool isSwitched = false;

  Widget build(BuildContext context) {
    //final bloc = Provider.of<carritoCompraBloc>(context);
    //_isData ? _showAlertDialog() : null;

    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        title: Text("Principal"),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            StreamBuilder(
              stream: ordenBloc.allOrdenProduct,
              builder: (context, AsyncSnapshot<OrdenModel> snapshot) {
                if (snapshot.hasData) {
                  ordenBloc.totalFuncion(true);
                  return buildList2(snapshot);
                } else if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                }
                return Center(child: CircularProgressIndicator());
              },
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                StreamBuilder<List<double>>(
                    stream: ordenBloc.getTotal,
                    initialData: [0.0, 0.0, 0.0],
                    builder: (BuildContext ctx,
                        AsyncSnapshot<List<double>> snapshot2) {
                      return RichText(
                        text: TextSpan(
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 14,
                          ),
                          children: [
                            TextSpan(
                              text: 'Subtotal: ',
                            ),
                            TextSpan(
                              text: '\$ ${snapshot2.data[0]} \n',
                              style: TextStyle(
                                  backgroundColor: Colors.blue[100],
                                  color: Colors.red),
                            ),
                            TextSpan(
                              text: 'Iva 12\% : ',
                            ),
                            TextSpan(
                              text: '\$ ${snapshot2.data[1]} \n',
                              style: TextStyle(
                                  backgroundColor: Colors.blue[100],
                                  color: Colors.red),
                            ),
                            TextSpan(
                              text: 'Total: ',
                            ),
                            TextSpan(
                              text: '\$ ${snapshot2.data[2]}',
                              style: TextStyle(
                                  backgroundColor: Colors.blue[100],
                                  color: Colors.red),
                            ),
                            TextSpan(
                              text: ' dolares',
                            ),
                          ],
                        ),
                      );
                    }),
                Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Center(
                          child: Text(
                            "¿Factura?",
                            style: new TextStyle(
                                color: Colors.deepOrange[900],
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Center(
                          child: Switch(
                            value: isSwitched,
                            onChanged: (value) {
                              ordenBloc.totalFuncion(false);
                              setState(() {
                                isSwitched = value;
                                print(isSwitched);
                              });
                            },
                            activeTrackColor: Colors.lightGreenAccent,
                            activeColor: Colors.green,
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0, right: 8.0),
                      child: RaisedButton(
                        onPressed: () {
                          //OJO SI FUNCIONA SOLO COMENTO PARA PROBAR DESDE OTRO PUNTO LA LLAMADA
                          //Navigator.pushNamed(context, '/ConfirmarPedido');
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (ctxt) => ConfirmarPedido(),
                            ),
                          );
                        },
                        child: Text("Confirmar Orden"),
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            //SizedBox(width: 10),
            //Icon(Icons.more_vert, size: 32, color: Colors.white),
          ],
        ),
      ),
    );
  }

  Widget buildList2(snapshot) {
    IpConfig local = new IpConfig("192.168.1.15", "5000");
    //IpConfig local = new IpConfig("35.247.235.167", "5000");
    print("llego a buildList2");
    //ordenBloc.totalFuncion(false);
    return Expanded(
      child: new ListView.builder(
        itemCount: snapshot.data.arrayProducto.length,
        itemBuilder: (BuildContext ctxt, int index) {
          return InkResponse(
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: Colors.white,
                      ),
                      child: ListTile(
                        onTap: () {},
                        leading:
                            snapshot.data.arrayProducto[index].urlImagen != null
                                ? Image.network(
                                    'http://${local.getIp()}:${local.getPort()}/imagenProducto/${snapshot.data.arrayProducto[index].id}${snapshot.data.arrayProducto[index].urlImagen}.jpg',
                                    fit: BoxFit.fill,
                                  )
                                : Text("No hay datos para mostrar"),
                        title: Text(
                          "${snapshot.data.arrayProducto[index].nombre}",
                          style: TextStyle(
                            //color: Theme.of(context).primaryColorDark,
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                        ),
                        subtitle: Text(
                          "${snapshot.data.arrayProducto[index].descripcionProducto}",
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                          ),
                        ),
                        trailing: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: CircleButton(
                                size: 27,
                                padding: 3,
                                icon: Icons.remove_circle_outline,
                                iconColor: Colors.red,
                                onTap: () => {
                                  ordenBloc.minusOrdenProductLocal(
                                      snapshot.data.arrayProducto[index]),
                                  snapshot.data.arrayProducto[index]
                                              .cantidadProducto ==
                                          0
                                      ? _showAlertDialog(index)
                                      : _isData = false,
                                  print(
                                      "Cantidad de este producto -> ${snapshot.data.arrayProducto[index].cantidadProducto}")
                                },
                              ),
                            ),
                            Expanded(
                              child: CircleButton(
                                size: 27,
                                padding: 3,
                                //icon: Icons.add,
                                icon: Icons.add_circle_outline,
                                iconColor: Colors.blue,
                                onTap: () => {
                                  ordenBloc.plusOrdenProductLocal(
                                      snapshot.data.arrayProducto[index])
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ItemPrecio(
                        false,
                        "\$ ${double.parse(snapshot.data.arrayProducto[index].precio)}",
                        13),
                    Text(
                      " CANTIDAD: ${snapshot.data.arrayProducto[index].cantidadProducto}\n "
                      "VALOR:${double.parse(snapshot.data.arrayProducto[index].precio) * snapshot.data.arrayProducto[index].cantidadProducto}",
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        //color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            onTap: () {
              print("Hola mundo ${snapshot.data.arrayProducto[index].nombre}");
            },
          );
        },
      ),
    );
  }
}

Widget checkoutListBuilder(snapshot) {
  return ListView.builder(
    itemCount: snapshot.data["cart_items"].length,
    itemBuilder: (BuildContext context, i) {
      final cartList = snapshot.data["cart_items"];
      return ListTile(
        title: Text(cartList[i]['name']),
        subtitle: Text("\$${cartList[i]['price']}"),
        trailing: IconButton(
          icon: Icon(Icons.remove_shopping_cart),
          onPressed: () {
            //bloc.removeFromCart(cartList[i]);
          },
        ),
        onTap: () {},
      );
    },
  );
}
