import 'dart:math';

import 'package:rioexpresscliente/adicional/search/flappy_search_bar.dart';
import 'package:rioexpresscliente/adicional/search/scaled_tile.dart';
import 'package:flutter/material.dart';

class Prueba extends StatelessWidget {
  Prueba({@required this.title});
  final title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[Text(title)],
        ),
      ),
    );
  }
}
