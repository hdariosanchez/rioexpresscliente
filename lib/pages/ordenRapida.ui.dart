import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rioexpresscliente/adicional/geoLocalization.dart';
import 'package:rioexpresscliente/bloc/carrito.bloc.dart';
import 'package:rioexpresscliente/bloc/direccion.bloc.dart';
import 'package:rioexpresscliente/models/direccion.model.dart';
//import 'package:rioexpresscliente/models/direccion.model.dart';
import 'package:rioexpresscliente/models/userLocation.model.dart';

import 'package:image_picker/image_picker.dart';

import 'package:toast/toast.dart';

/*COMPONENTES DE ANIMACION*/
final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class OrdenRapida extends StatefulWidget {
  @override
  _OrdenRapidaState createState() => _OrdenRapidaState();
}

class _OrdenRapidaState extends State<OrdenRapida> {
  File _image;
  final picker = ImagePicker();

  var lat = "";
  var lng = "";
  bool loaderFlag = false;

  String numeroVivenEnVivienda = 'DIRECCION GUARDADA 1';
  String selectedDireccionInicio;
  String selectedDireccionFin;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _controllerTitle = TextEditingController();
  bool _isLoading = false;

  void _upload() {
    if (_image == null) return;
    String base64Image = base64Encode(_image.readAsBytesSync());
    String fileName = _image.path.split("/").last;
  }

  Future getImage(int opcion) async {
    //
    switch (opcion) {
      case 1:
        {
          final pickedFile = await picker.getImage(source: ImageSource.gallery);
          if (pickedFile != null) {
            setState(() {
              _image = File(pickedFile.path);
            });
          }
        }
        break;
      case 2:
        {
          final pickedFile = await picker.getImage(source: ImageSource.camera);
          if (pickedFile != null) {
            setState(() {
              _image = File(pickedFile.path);
            });
          }
        }
        break;
    }

    bool switchControl = false;
    var textHolder = 'Switch is OFF';

    void toggleSwitch() {
      if (switchControl == false) {
        setState(() {
          switchControl = true;
          textHolder = 'Switch is ON';
        });
        print('Switch is ON');
        // Put your code here which you want to execute on Switch ON event.

      } else {
        setState(() {
          switchControl = false;
          textHolder = 'Switch is OFF';
        });
        print('Switch is OFF');
        // Put your code here which you want to execute on Switch OFF event.
      }
    }

    /*
    PermissionStatus permissionResult =
        await SimplePermissions.requestPermission(
            Permission.WriteExternalStorage);
    if (permissionResult == PermissionStatus.authorized) {
      // code of read or write file in external storage (SD card)
      final pickedFile = await picker.getImage(source: ImageSource.gallery);
      setState(() {
        _image = File(pickedFile.path);
      });
    } else {
      _showAlertDialogInfo();
    }
    */
  }

  void _showAlertDialogInfo() {
    showDialog(
      context: context,
      builder: (_) => Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)), //this right here
        child: Container(
          height: 190,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset(
                    'assets/image/icon-camera-foto.png',
                    width: 50.0,
                  ),
                ),
                Text(
                  "No ha concedido permisos para obtener una imagen",
                  style: new TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            //getImage();
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancelar",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFFe27468),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Reintentar",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFF1BC0C5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showAlertDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)), //this right here
        child: Container(
          height: 190,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset(
                    'assets/image/icon-camera-foto.png',
                    width: 50.0,
                  ),
                ),
                Text(
                  "Desea agregar una imagen desde Galeria o tomar una fotografia",
                  style: new TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            getImage(2);
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Camara",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFFe27468),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          onPressed: () {
                            getImage(1);
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Galleria",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: const Color(0xFF1BC0C5),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    direccionBloc.paginaOrdenRapida(true);
//    direccionBloc.fetchAllMyDirections("5ede20bf8b907b087a2be5aa");
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Image.network(
          "https://www.todoiphone.net/wp-content/uploads/2014/03/WhatsApp-Wallpaper-28.png",
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.cover,
        ),
        Scaffold(
          key: _scaffoldState,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            title: Text("Orden Rapida"),
          ),
          body: SingleChildScrollView(
            child: Container(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Image.asset(
                        'assets/image/delivery_fast.png',
                        width: 60.0,
                      ),
                    ),
                    new TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.account_balance_wallet),
                        hintText: 'Ingrese cédula o Ruc',
                        labelText: 'RUC O CÉDULA',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Por favor ingrese un numero válido';
                        }
                      },
                      keyboardType: TextInputType.number,
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.person),
                        hintText: 'Ingrese nombre y apellido',
                        labelText: 'NOMBRES Y APELLIDOS',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Por favor ingrese nombres válidos';
                        }
                      },
                    ),
                    new TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.phone_android),
                        hintText: 'Ingrese su número de celular',
                        labelText: 'CELULAR',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Por favor ingrese celular válido';
                        }
                      },
                      keyboardType: TextInputType.phone,
                    ),
                    new TextFormField(
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.speaker_phone),
                        hintText: 'Ingrese numero de familiar o amigo',
                        labelText: 'CELULAR ADICIONAL',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Por favor ingrese celular válido';
                        }
                      },
                      //keyboardType: TextInputType.emailAddress,
                      keyboardType: TextInputType.phone,
                    ),
                    new TextFormField(
                      maxLines: 3,
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.swap_horizontal_circle),
                        hintText:
                            'Ej. Desde la condamine hasta Sector Politecnica Ciudadela Juan Montalvo Calles X y XY',
                        labelText: 'DIRECCION INICIAL Y FINAL',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Por favor ingrese datos validos';
                        }
                      },
                      //keyboardType: TextInputType.emailAddress,
                      keyboardType: TextInputType.text,
                    ),
                    new TextFormField(
                      maxLines: 4,
                      decoration: const InputDecoration(
                        icon: const Icon(Icons.add_comment),
                        hintText:
                            'Ej. Necesito entregar documentos al Sr. XY en el punto de entrega. mas detalles via telefono por favor',
                        labelText: 'DESEO LO SIGUIENTE',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Por favor ingrese datos válido';
                        }
                      },
                      //keyboardType: TextInputType.emailAddress,
                      keyboardType: TextInputType.text,
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 12.0, top: 12.0, right: 12.0, bottom: 0.0),
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: 75,
                            height: 75,
                            child: Image.asset('assets/image/googlemap.png'),
                          ),
                          new FlatButton.icon(
                            //color: Colors.cyanAccent[80],
                            textColor: Colors.black,
                            splashColor: Colors.blueAccent,
                            icon: const Icon(
                              Icons.radio_button_checked,
                              size: 18.0,
                              color: Colors.blueAccent,
                            ),
                            label: const Text(
                              'Opcional (Ubicación en mapa)',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                                decorationStyle: TextDecorationStyle.solid,
                              ),
                            ),
                            onPressed: () async {
                              if (direccionBloc.getCurrentPositionInicio !=
                                      null &&
                                  direccionBloc.getCurrentPositionFin != null) {
                                direccionBloc.paginaOrdenRapida(true);
                              }
                              Navigator.pushNamed(context, '/addMiDireccion');
                            },
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        StreamBuilder<LatLng>(
                            stream: direccionBloc.positionInicio,
                            initialData: null,
                            builder: (BuildContext context,
                                AsyncSnapshot<LatLng> snapshot) {
                              return Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(0),
                                    decoration: snapshot.data == null
                                        ? BoxDecoration(
                                            color: Colors.red,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          )
                                        : BoxDecoration(
                                            color: Colors.green,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                    constraints: BoxConstraints(
                                      minWidth: 18,
                                      minHeight: 18,
                                    ),
                                    child: snapshot.data == null
                                        ? Icon(
                                            Icons.error_outline,
                                            size: 16.0,
                                            color: Colors.white,
                                          )
                                        : Icon(
                                            Icons.check_circle_outline,
                                            size: 16.0,
                                            color: Colors.white,
                                          ),
                                  ),
                                  snapshot.data == null
                                      ? Text("Sin Coordenada Inicio")
                                      : Text("Coordenada Inicio"),
                                ],
                              );
                            }),
                        StreamBuilder<LatLng>(
                            stream: direccionBloc.positionFin,
                            initialData: null,
                            builder: (BuildContext context,
                                AsyncSnapshot<LatLng> snapshot) {
                              return Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(0),
                                    decoration: snapshot.data == null
                                        ? BoxDecoration(
                                            color: Colors.red,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          )
                                        : BoxDecoration(
                                            color: Colors.green,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                    constraints: BoxConstraints(
                                      minWidth: 18,
                                      minHeight: 18,
                                    ),
                                    child: snapshot.data == null
                                        ? Icon(
                                            Icons.error_outline,
                                            size: 16.0,
                                            color: Colors.white,
                                          )
                                        : Icon(
                                            Icons.check_circle_outline,
                                            size: 16.0,
                                            color: Colors.white,
                                          ),
                                  ),
                                  snapshot.data == null
                                      ? Text("Sin Coordenada Fin")
                                      : Text("Coordenada Fin"),
                                ],
                              );
                            }),
                      ],
                    ),

                    /*
                    Transform.scale(
                        scale: 1.5,
                        child: Switch(
                          onChanged: (bool vale) => {},
                          value: false,
                          activeColor: Colors.blue,
                          activeTrackColor: Colors.green,
                          inactiveThumbColor: Colors.white,
                          inactiveTrackColor: Colors.grey,
                        )),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 12.0, top: 12.0, right: 12.0, bottom: 0.0),
                      child: Text(
                        "RECOGER EN",
                        style: TextStyle(
                          //color: Colors.black38,
                          fontSize: 15.0,
                          fontWeight: FontWeight.normal,
                        ),
                        textAlign: TextAlign.justify,
                        textDirection: TextDirection.rtl,
                      ),
                    ),
                    FutureBuilder<MiDireccionModelList>(
                      future: direccionBloc.fetchAllMyDirectionsForDropDow(),
                      builder: (BuildContext context,
                          AsyncSnapshot<MiDireccionModelList> snapshot) {
                        if (!snapshot.hasData)
                          return CircularProgressIndicator();
                        return Padding(
                          padding: EdgeInsets.only(
                              left: 12.0, top: 0.0, right: 12.0, bottom: 0.0),
                          child: DropdownButton<String>(
                            hint: new Text("Seleccione Ubicacion Inicio"),
                            value: selectedDireccionInicio,
                            icon: Icon(Icons.arrow_downward),
                            isDense: true,
                            iconSize: 24,
                            elevation: 16,
                            isExpanded: true,
                            style: TextStyle(color: Colors.deepPurple),
                            underline: Container(
                              height: 3,
                              color: Colors.deepPurpleAccent,
                            ),
                            items: snapshot.data.results
                                .map((miDireccion) => DropdownMenuItem<String>(
                                      child: Text(miDireccion.nombre),
                                      value: miDireccion.id,
                                    ))
                                .toList(),
                            onChanged: (String value) {
                              setState(() {
                                selectedDireccionInicio = value;
                              });

                              print("viendo que direccion escojo--> $value");
                            },
                          ),
                        );
                      },
                    ),
                    
                    Padding(
                      padding: EdgeInsets.only(
                          left: 12.0, top: 12.0, right: 12.0, bottom: 0.0),
                      child: Text(
                        "ENTREGAR EN",
                        style: TextStyle(
                          //color: Colors.black38,
                          fontSize: 15.0,
                          fontWeight: FontWeight.normal,
                        ),
                        textAlign: TextAlign.justify,
                        textDirection: TextDirection.rtl,
                      ),
                    ),
                    FutureBuilder<MiDireccionModelList>(
                      future: direccionBloc.fetchAllMyDirectionsForDropDow(),
                      builder: (BuildContext context,
                          AsyncSnapshot<MiDireccionModelList> snapshot) {
                        if (!snapshot.hasData)
                          return CircularProgressIndicator();
                        return Padding(
                          padding: EdgeInsets.only(
                              left: 12.0, top: 0.0, right: 12.0, bottom: 0.0),
                          child: DropdownButton<String>(
                            hint: new Text("Seleccione Ubicacion Fin"),
                            value: selectedDireccionFin,
                            icon: Icon(Icons.arrow_downward),
                            isDense: true,
                            iconSize: 24,
                            elevation: 16,
                            isExpanded: true,
                            style: TextStyle(color: Colors.deepPurple),
                            underline: Container(
                              height: 3,
                              color: Colors.deepPurpleAccent,
                            ),
                            items: snapshot.data.results
                                .map((miDireccion) => DropdownMenuItem<String>(
                                      child: Text(miDireccion.nombre),
                                      value: miDireccion.id,
                                    ))
                                .toList(),
                            onChanged: (String value) {
                              setState(() {
                                selectedDireccionFin = value;
                              });

                              print("viendo que direccion escojo--> $value");
                            },
                          ),
                        );
                      },
                    ),
                    */
                    Padding(
                      padding: EdgeInsets.only(
                          left: 12.0, top: 12.0, right: 12.0, bottom: 0.0),
                      child: Text(
                        "AGREGUE UNA IMAGEN REFERENCIAL",
                        style: TextStyle(
                          //color: Colors.black38,
                          fontSize: 15.0,
                          fontWeight: FontWeight.normal,
                        ),
                        textAlign: TextAlign.justify,
                        textDirection: TextDirection.rtl,
                      ),
                    ),
                    Center(
                      child: _image == null
                          ? Text(
                              "No tiene ninguna imagen agregada",
                              style: new TextStyle(
                                  color: Colors.black,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.w500),
                            )
                          : Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0)),
                                color: const Color(0xFF1BC0C5),
                              ),
                              child: Image.file(
                                _image,
                                height:
                                    MediaQuery.of(context).size.height * 0.38,
                                width: MediaQuery.of(context).size.width * 0.9,
                              ),
                            ),
                    ),
                    lat == ""
                        ? Text(
                            "No has actualizado tu ubicación, presiona actualizar para poder localizarte inmediatamente en caso de emergencia",
                            style: TextStyle(
                              color: Colors.black54,
                              fontSize: 12.0,
                              fontWeight: FontWeight.normal,
                            ),
                          )
                        //: Text("Trajo la ubicacion $lat y $lng"),
                        : Text(
                            "Se ha encontrado tu localizacion exitosamente $lng y $lat",
                            style: TextStyle(
                              color: Colors.black54,
                              fontSize: 12.0,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                    Center(
                      child: RaisedButton(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            // Si el formulario es válido, queremos mostrar un Snackbar
                            setState(() => _isLoading = true);
                            String idCliente = "5ede20bf8b907b087a2be5aa";
                            String nombreApellido =
                                "HERMES DARIO SANCHEZ BERMEO";
                            double latitude = 0.0;
                            double longitude = 1.1;
/*
                            MiDireccion direccion = new MiDireccion();
                            direccion.id = "";
                            direccion.idCliente = userId;
                            direccion.nombre = title;
                            direccion.longitud = longitude;
                            direccion.latitud = latitude;
*/

                          } else {
                            _scaffoldState.currentState.showSnackBar(
                              SnackBar(
                                content: Text("Por favor rellene los campos"),
                              ),
                            );
                            return;
                          }
                        },
                        child: Text(
                          "ORDENAR PEDIDO".toUpperCase(),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        color: Color(0xFFD81B60),
                      ),
                    ),
                    _isLoading ? CircularProgressIndicator() : Container(),
                  ],
                ),
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => {
              _showAlertDialog()
              //getImage()
            },
            tooltip: 'Pick Image',
            child: Icon(Icons.add_a_photo),
          ),
        ),
      ],
    );
  }

  Future getLocation() async {
    setState(() {
      loaderFlag = true;
    });
    UserLocation userLocation = await LocationService().getLocation();
    Toast.show(
        "Toast plugin app -->" + userLocation.longitude.toString(), context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    setState(() {
      loaderFlag = false;
      lat = userLocation.longitude.toString();
      lng = userLocation.latitude.toString();
    });
  }

  // user defined function
  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Alert Dialog title"),
          content: new Text("Alert Dialog body"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
