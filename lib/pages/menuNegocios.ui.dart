/****
 * 
import 'package:flutter/material.dart';
import 'package:rioexpresscliente/pages/ordenesList.ui.dart';

import 'negocio.ui.dart';

class MenuNegocios extends StatefulWidget {
  @override
  _MenuNegociosState createState() => new _MenuNegociosState();
}

class _MenuNegociosState extends State<MenuNegocios> {
  int _count = 0;
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    print("index----> $index");
    MaterialPageRoute(builder: (_) => OrdenesList());
    setState(
      () {
        _selectedIndex = index;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Establecimientos"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              new Container(
                child: new Column(
                  children: <Widget>[
                    ClipOval(
                      child: Material(
                        color: Colors.white, // button color
                        child: InkWell(
                          splashColor: Colors.red, // inkwell color
                          child: SizedBox(
                            width: 75,
                            height: 75,
                            child: Image.asset('assets/image/ico_comidas.png'),
                          ),
                          onTap: () {
                            //Navigator.pushNamed(context, '/negocios', arguments:"Restaurante");

                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => NegocioItems(
                                  tipoNegocio: "Restaurante",
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: new Text(
                        "Restaurante",
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                child: new Column(
                  children: <Widget>[
                    ClipOval(
                      child: Material(
                        color: Colors.white, // button color
                        child: InkWell(
                          splashColor: Colors.red, // inkwell color
                          child: SizedBox(
                            width: 75,
                            height: 75,
                            child: Image.asset('assets/image/ico_farmacia.png'),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => NegocioItems(
                                  tipoNegocio: "Farmacia",
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: new Text(
                        "Farmacia",
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              new Container(
                child: new Column(
                  children: <Widget>[
                    ClipOval(
                      child: Material(
                        color: Colors.white, // button color
                        child: InkWell(
                          splashColor: Colors.red, // inkwell color
                          child: SizedBox(
                            width: 75,
                            height: 75,
                            child: Image.asset(
                                'assets/image/ico_licores_bebidas.png'),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => NegocioItems(
                                  tipoNegocio: "Licoreria",
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: new Text(
                        "Licores",
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                child: new Column(
                  children: <Widget>[
                    ClipOval(
                      child: Material(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(50.0),
                          bottomLeft: Radius.circular(50.0),
                        ),
                        color: Colors.white, // button color
                        child: InkWell(
                          splashColor: Colors.red, // inkwell color
                          child: SizedBox(
                            width: 75,
                            height: 75,
                            child:
                                Image.asset('assets/image/ico_regalos_mas.png'),
                          ),
                          onTap: () {
                            //Navigator.pushNamed(context, '/negocios', arguments:"Regalos");

                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => NegocioItems(
                                  tipoNegocio: "Regalos",
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: new Text(
                        "Regalos y mas",
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              new Container(
                child: new Column(
                  children: <Widget>[
                    ClipOval(
                      child: Material(
                        color: Colors.white, // button color
                        child: InkWell(
                          splashColor: Colors.red, // inkwell color
                          child: SizedBox(
                            width: 75,
                            height: 75,
                            child: Image.asset('assets/image/ico_market.png'),
                          ),
                          onTap: () {
                            //Navigator.pushNamed(context, '/negocios');
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => NegocioItems(
                                  tipoNegocio: "Market",
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: new Text(
                        "Market",
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                child: new Column(
                  children: <Widget>[
                    ClipOval(
                      child: Material(
                        color: Colors.white, // button color
                        child: InkWell(
                          splashColor: Colors.red, // inkwell color
                          child: SizedBox(
                            width: 75,
                            height: 75,
                            child: Image.asset(
                                'assets/image/ico_panaderia_pasteleria.png'),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => NegocioItems(
                                  tipoNegocio: "Panaderia",
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: new Text(
                        "Panaderia y mas",
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              new Container(
                child: new Column(
                  children: <Widget>[
                    ClipOval(
                      child: Material(
                        color: Colors.white, // button color
                        child: InkWell(
                          splashColor: Colors.red, // inkwell color
                          child: SizedBox(
                            width: 75,
                            height: 75,
                            child:
                                Image.asset('assets/image/ico_papeleria.png'),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => NegocioItems(
                                  tipoNegocio: "Papeleria",
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: new Text(
                        "Papeleria",
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                child: new Column(
                  children: <Widget>[
                    ClipOval(
                      child: Material(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(50.0),
                          bottomLeft: Radius.circular(50.0),
                        ),
                        color: Colors.white, // button color
                        child: InkWell(
                          splashColor: Colors.red, // inkwell color
                          child: SizedBox(
                            width: 75,
                            height: 75,
                            child:
                                Image.asset('assets/image/ico_tecnologia.png'),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => NegocioItems(
                                  tipoNegocio: "Tecnologia",
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: new Text(
                        "Técnologia",
                        style: new TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

 */
