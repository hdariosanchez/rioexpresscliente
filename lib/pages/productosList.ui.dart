import "package:flutter/material.dart";
import 'package:rioexpresscliente/models/ordenFactura.model.dart';

import 'package:rioexpresscliente/components/buttons/circle_button.dart';
import 'package:rioexpresscliente/configuration/local.dart';

class ProductosList extends StatefulWidget {
  ProductosList({Key key, this.productosList}) : super(key: key);
  //ProductosList({Key key}) : super(key: key);
  final List<ListProductFacturado> productosList;

  @override
  _ProductoListState createState() => _ProductoListState();
}

class _ProductoListState extends State<ProductosList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Producto Ordenes"),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              //sharedPreferences.clear();
              /*
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()),
                  (Route<dynamic> route) => false);
                  */
            },
            child:
                Text("Solo por poner", style: TextStyle(color: Colors.black)),
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          buildList2(widget.productosList),
        ],
      ),
    );
  }

  Widget buildList2(snapshot) {
    IpConfig local = new IpConfig("192.168.1.15", "5000");
    //IpConfig local = new IpConfig("35.247.235.167", "5000");
    print("llego a buildList2");

    return new Expanded(
      child: new ListView.builder(
        itemCount: snapshot.length,
        itemBuilder: (BuildContext ctxt, int index) {
          return InkResponse(
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: Colors.white,
                      ),
                      child: ListTile(
                        onTap: () {},
                        leading: snapshot[index].urlImagen != null
                            ? Image.network(
                                'http://${local.getIp()}:${local.getPort()}/imagenProducto/${snapshot[index].id}${snapshot[index].urlImagen}.jpg',
                                fit: BoxFit.fill,
                              )
                            : CircularProgressIndicator(),
                        title: Text(
                          "${snapshot[index].nombre}",
                          style: TextStyle(
                            //color: Theme.of(context).primaryColorDark,
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                        ),
                        subtitle: Text(
                          "${snapshot[index].descripcionProducto}\n" +
                              "\$ ${double.parse(snapshot[index].precio)}\n" +
                              " CANTIDAD: ${snapshot[index].cantidadProducto}\n" +
                              "VALOR:${double.parse(snapshot[index].precio) * snapshot[index].cantidadProducto}",
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                          ),
                        ),
                        /*
                        trailing: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: CircleButton(
                                size: 27,
                                padding: 3,
                                icon: Icons.remove_circle_outline,
                                iconColor: Colors.red,
                                onTap: () => {

                                },
                              ),
                            ),
                          ],
                        ),
                        */
                      ),
                    ),
                  ],
                ),
              ),
            ),
            onTap: () {
              print("Hola mundo ${snapshot[index].nombre}");
            },
          );
        },
      ),
    );
  }
}
