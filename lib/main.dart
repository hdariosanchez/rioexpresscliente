import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rioexpresscliente/pages/addMiDireccion.ui.dart';
import 'package:rioexpresscliente/pages/carritoCompra.ui.dart';
import 'package:rioexpresscliente/pages/createMiDireccion.ui.dart';
import 'package:rioexpresscliente/pages/direccionList.ui.dart';
import 'package:rioexpresscliente/pages/negocio.ui.dart';
import 'package:rioexpresscliente/pages/ordenRapida.ui.dart';
import 'package:rioexpresscliente/pages/ordenesList.ui.dart';
import 'package:rioexpresscliente/pages/principal.dart';
import 'package:rioexpresscliente/pages/productoNegocio.ui.dart';
import 'package:rioexpresscliente/bloc/blocCarritoCompra.dart';
import 'package:rioexpresscliente/pages/prueba.dart';
import 'package:rioexpresscliente/pages/menuNegocios.ui.dart';
import 'package:rioexpresscliente/pages/menuPrincipal.ui.dart';
import 'package:rioexpresscliente/pages/login_page.ui.dart';
import 'package:rioexpresscliente/pages/confirmarPedido.ui.dart';
import 'package:shared_preferences/shared_preferences.dart';

final navigatorKey = GlobalKey<NavigatorState>();

void main() {
  runApp(MyAppRioexpressClient());
}

class MyAppRioexpressClient extends StatefulWidget {
  @override
  _MyAppRioexpressClientState createState() =>
      new _MyAppRioexpressClientState();
}

class _MyAppRioexpressClientState extends State<MyAppRioexpressClient> {
  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  SharedPreferences sharedPreferences;

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      MaterialPageRoute(builder: (_) => LoginPage());
    } else {
      MaterialPageRoute(builder: (BuildContext context) => Principal());
    }
  }

//class MyAppRioexpressClient extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    '/': (BuildContext context) => LoginPage(), //MenuPrincipal(),
    '/negocios': (BuildContext context) => NegocioItems(),
    '/principal': (BuildContext context) => Principal(),
    '/carritoCompra': (BuildContext context) => CarritoCompra(),
    '/negocioProductos': (BuildContext context) => NegocioProductosItems(),
    '/prueba': (BuildContext context) => Prueba(
          title: "Hola desde el main",
        ),
    //'/menuNegocio': (BuildContext context) => MenuNegocios(),
    '/miDireccion': (BuildContext context) => MiDireccionP(),
    '/addMiDireccion': (BuildContext context) => AddMiDireccion(),
    '/createMiDireccion': (BuildContext context) => CreateMiDireccion(),
    '/OrdenRapida': (BuildContext context) => OrdenRapida(),
    '/OrdenesList': (BuildContext context) => OrdenesList(title: "Home Page"),
    '/ConfirmarPedido': (BuildContext context) => ConfirmarPedido(),
  };

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return
        //ChangeNotifierProvider<carritoCompraBloc>(
        ChangeNotifierProvider(
      create: (context) => carritoCompraBloc(),
      child: MaterialApp(
        navigatorKey: navigatorKey,
        debugShowCheckedModeBanner: false,
        title: 'RIOEXPRESS',
        theme: ThemeData(
          primarySwatch: Colors.yellow,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        //initialRoute: '/',
        home: LoginPage(),
        //routes: routes,
      ),
    );
  }
}
