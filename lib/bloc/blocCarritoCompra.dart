/// The [dart:async] is neccessary for using streams
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:rioexpresscliente/bloc/productoNegocio.bloc.dart';

class carritoCompraBloc with ChangeNotifier {
  /// The [cartStreamController] is an object of the StreamController class
  /// .broadcast enables the stream to be read in multiple screens of our app
  final cartStreamController = StreamController.broadcast();

  /// The [getStream] getter would be used to expose our stream to other classes
  Stream get getStream => cartStreamController.stream;

  int _cambia = 10;

/*
  get cambia {
    return productoBloc.obtenerCantidadItemCar();
  }
*/
  set cambia(int numeroSuma) {
    this._cambia = _cambia + numeroSuma;
    //notifyListeners();
  }

  final Map allItems = {
    'shop_items': [
      {'name': 'App dev kit', 'price': 20, 'id': 1},
      {'name': 'App consultation', 'price': 100, 'id': 2},
      {'name': 'Logo Design', 'price': 10, 'id': 3},
      {'name': 'Code review', 'price': 90, 'id': 4},
    ],
    'cart_items': []
  };

  void addToCart(item) {
    _cambia = 1;
    allItems['shop_items'].remove(item);
    allItems['cart_items'].add(item);
    cartStreamController.sink.add(allItems);
    notifyListeners();
  }

  void prueba() {
    notifyListeners();
  }

  void removeFromCart(item) {
    _cambia = 2;
    allItems['cart_items'].remove(item);
    allItems['shop_items'].add(item);
    cartStreamController.sink.add(allItems);
    notifyListeners();
  }

  /// The [dispose] method is used
  /// to automatically close the stream when the widget is removed from the widget tree
  void dispose() {
    cartStreamController.close(); // close our StreamController
  }
}

final bloc = carritoCompraBloc();
