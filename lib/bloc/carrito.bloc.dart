import 'dart:convert';

import 'package:rioexpresscliente/models/producto.model.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rioexpresscliente/models/orden.model.dart';
import 'package:rioexpresscliente/resources/repository.orden.dart';
import 'package:rioexpresscliente/resources/repository.factura.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rioexpresscliente/models/factura.model.dart';

class OrdenBloc {
  final _repositoryFactura = RepositoryFactura();
  final _repository = RepositoryOrden();
  final _ordenFetcher = new PublishSubject<OrdenModel>();
  final _ordenNumberFetcher = new PublishSubject<int>();
  final _ordenTotalFetcher = new PublishSubject<List<double>>();
  final _newOrdenFetcher = new PublishSubject<bool>();
  final _miFacturaCreate = new PublishSubject<bool>();

  SharedPreferences sharedPreferences;

  int initialNumberCar = 0;
  String idOrden = "";
  OrdenModel ordenModelLocal = new OrdenModel();
  //ordenModelLocal.arrayProductoCreate([]);
  double subtotal = 00.0;
  List<double> calculoValor = [0.0, 0.0, 0.0];
  bool _ivaCalculoLocal = true;

  Stream<OrdenModel> get allOrdenProduct => _ordenFetcher.stream;
  Stream<int> get getNumberCar => _ordenNumberFetcher.stream;
  Stream<List<double>> get getTotal => _ordenTotalFetcher.stream;

  Stream<bool> get createFacturaBool => _miFacturaCreate.stream;

  get getNumberCarCurrent => initialNumberCar;
  Stream<bool> get getNewOrdenCreate => _newOrdenFetcher.stream;

  fetchAllOrdenProduct() async {
    sharedPreferences = await SharedPreferences.getInstance();
    //idOrden = "5f25e1e30992ae076bf7451b";
    var idUser = sharedPreferences.getString("idUser");
    OrdenModel ordenModel = ordenModelLocal =
        await _repository.fetchAllProductoOrdenByNegocio(idUser);
    print("Viendo que llega en Orden $ordenModelLocal");
    if (ordenModelLocal != null) {
      initialNumberCar = ordenModelLocal.arrayProducto.length;
      idOrden = ordenModelLocal.id;
      sharedPreferences.setString("idOrden", ordenModelLocal.id);
      //initialNumberCar = 0;
    } else {
      initialNumberCar = 0;
    }
    _ordenFetcher.sink.add(ordenModelLocal);
    _ordenNumberFetcher.sink.add(initialNumberCar);
    //_ordenTotalFetcher
  }

  totalFuncion(bool _ivaCalculo) async {
    print("se ejecuto el iva en --> $_ivaCalculo");
    _ivaCalculoLocal = _ivaCalculo;
    double subtotal = 0.0;
    for (int i = 0; i < ordenModelLocal.arrayProducto.length; i++) {
      subtotal = (double.parse(ordenModelLocal.arrayProducto[i].precio) *
              ordenModelLocal.arrayProducto[i].cantidadProducto) +
          subtotal;
      if (i == ordenModelLocal.arrayProducto.length - 1) {
        calculoValor[0] = double.parse(subtotal.toStringAsFixed(2));
        if (_ivaCalculo) {
          calculoValor[1] = double.parse((subtotal * 0.12).toStringAsFixed(2));
          calculoValor[2] =
              double.parse((subtotal + (subtotal * 0.12)).toStringAsFixed(2));
        } else {
          calculoValor[1] = 0.00;
          calculoValor[2] = double.parse(subtotal.toStringAsFixed(2));
        }
        print("Estamos incentivando el _ordenTotalFetcher");
        _ordenTotalFetcher.sink.add(calculoValor);
      }
    }
  }

  saveDatosFactura(
      String rucCedula,
      String nombreApellido,
      String celular,
      String celularAdicional,
      String direccionEntrega,
      String observaciones,
      double latitud,
      double longitud) async {
    sharedPreferences = await SharedPreferences.getInstance();
    String idOrden = sharedPreferences.getString("idOrden");
    String idUser = sharedPreferences.getString("idUser");

    Factura factura = new Factura();

    factura.rucCedula = rucCedula;
    factura.nombreApellido = nombreApellido;
    factura.celular = celular;
    factura.celularAdicional = celularAdicional;
    factura.direccionEntrega = direccionEntrega;
    factura.observaciones = observaciones;
    factura.idOrden = idOrden;
    factura.idUser = idUser;
    factura.subTotal = calculoValor[0].toString();
    factura.iva = calculoValor[1].toString();
    factura.total = calculoValor[2].toString();
    factura.latitud = latitud;
    factura.longitud = longitud;
    bool cFacturaBool = await _repositoryFactura.ingresoNuevaFactura(factura);
    _miFacturaCreate.sink.add(cFacturaBool);
    //return ();
  }

  createOrden(ProductoModel nuevoProducto, String idNegocio) async {
    sharedPreferences = await SharedPreferences.getInstance();
    //sharedPreferences.setString("idNegocio", idNegocio);
    //idOrden = "5f25e1e30992ae076bf7451b";
    var idUser = sharedPreferences.getString("idUser");
    if (idOrden == "") {
      OrdenModel ordenModel = new OrdenModel();
      ordenModel.arrayProductoCreate = nuevoProducto;
      ordenModel.idCliente = idUser;
      ordenModel.estado = "ACTIVO";
      ordenModel.tipoOrden = "NORMAL";

      String ingresoCorrecto = await _repository.ingresoNuevaOrden(ordenModel);
      sharedPreferences.setString("idNegocio", idNegocio);
      idOrden = ingresoCorrecto;
      initialNumberCar = 1;
      _ordenNumberFetcher.sink.add(initialNumberCar);
      _newOrdenFetcher.sink.add(true);
      fetchAllOrdenProduct();
    } else {
      var idNegocioShared = sharedPreferences.getString("idNegocio");
      if (idNegocioShared == idNegocio) {
        String targetId = nuevoProducto.id;
        var index = ordenModelLocal.arrayProducto
            .indexWhere((obj) => obj.id == targetId);
        if (index >= 0) {
          print("YA EXISTE ESTE PRODUCTO--> $index");
          _newOrdenFetcher.sink.add(false);
        } else {
          bool ingresoCorrecto = await _repository.ingresoNuevoProductoOrden(
              nuevoProducto, idOrden);
          if (ingresoCorrecto) {
            ListProductCar obj =
                //new ListProductCar(json.encode(nuevoProducto), true);
                new ListProductCar(nuevoProducto, false);

            ordenModelLocal.arrayProducto.add(obj);
            _ordenNumberFetcher.sink.add(ordenModelLocal.arrayProducto.length);
            try {} catch (e) {
              print("Hubo error en el ingresoCorrecto");
            }
          }
          _newOrdenFetcher.sink.add(ingresoCorrecto);
        }
      } else {
        _newOrdenFetcher.sink.add(false);
      }
    }
  }

  minusOrdenProductLocal(ListProductCar itemProducto) async {
    print("${itemProducto.cantidadProducto}");
    String targetId = itemProducto.id;
    var index =
        ordenModelLocal.arrayProducto.indexWhere((obj) => obj.id == targetId);
    if (itemProducto.cantidadProducto > 0) {
      itemProducto.setCantidadProducto = itemProducto.cantidadProducto - 1;
      totalFuncion(_ivaCalculoLocal);
      _ordenFetcher.sink.add(ordenModelLocal);
    } else {
      //deleteProductoLocal(index);
    }
  }

  deleteProductoLocal(int index, bool removed) async {
    if (removed) {
      ordenModelLocal.arrayProducto.removeAt(index);
      totalFuncion(_ivaCalculoLocal);
      _ordenFetcher.sink.add(ordenModelLocal);
    } else {
      ordenModelLocal.arrayProducto[index].setCantidadProducto =
          ordenModelLocal.arrayProducto[index].cantidadProducto + 1;
      totalFuncion(_ivaCalculoLocal);
      _ordenFetcher.sink.add(ordenModelLocal);
    }
  }

  plusOrdenProductLocal(ListProductCar itemProducto) async {
    print("${itemProducto.cantidadProducto}");
    itemProducto.setCantidadProducto = itemProducto.cantidadProducto + 1;
    totalFuncion(_ivaCalculoLocal);
    _ordenFetcher.sink.add(ordenModelLocal);
  }

  fetchAllOrdenProductLocal() async {
    print(
        "llego a pedir la orden si necesidad de la red--> ${ordenModelLocal.arrayProducto}");
    _ordenFetcher.sink.add(ordenModelLocal);

    print("jdsfjlajsdfl");
    _ordenFetcher.sink.add(ordenModelLocal);
  }

  dispose() {
    _ordenFetcher.close();
    _ordenNumberFetcher.close();
    _newOrdenFetcher.close();
    _ordenTotalFetcher.close();
    _miFacturaCreate.close();
  }
}

final ordenBloc = OrdenBloc();
