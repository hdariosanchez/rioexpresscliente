import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rioexpresscliente/models/ordenFactura.model.dart';
import 'package:rxdart/rxdart.dart';

import 'package:rioexpresscliente/resources/repository.ordenesFactura.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrdenesFactura {
  final _repository = RepositoryOrdenesFactura();
  final _ordenFetcher = new PublishSubject<OrdenFacturaModelList>();
  final _miDireccionDelete = new PublishSubject<bool>();
  final _miDireccionCreate = new PublishSubject<bool>();
  final _myCurrentPosition = new PublishSubject<LatLng>();
  final _positionInicio = new PublishSubject<LatLng>();
  final _positionEntregaOrden = new PublishSubject<LatLng>();
  final _positionFin = new PublishSubject<LatLng>();
  LatLng currentPositionObj;
  LatLng currentPositionInicio = null;
  LatLng currentPositionEntregaOrden = null;
  LatLng currentPositionFin = null;
  bool _paginaOrdenRapida;

  SharedPreferences sharedPreferences;

  Stream<OrdenFacturaModelList> get allMyOrdensPendiente =>
      _ordenFetcher.stream;

  Stream<bool> get deleteMyDirectionBool => _miDireccionDelete.stream;

  Stream<bool> get createDirectionBool => _miDireccionCreate.stream;

  Stream<LatLng> get currentPositionLatLng => _myCurrentPosition.stream;
  Stream<LatLng> get positionInicio => _positionInicio.stream;
  Stream<LatLng> get positionEntregaOrden => _positionEntregaOrden.stream;
  Stream<LatLng> get positionFin => _positionFin.stream;
  LatLng get currentPositionLatLngSimple => currentPositionObj;
  LatLng get getCurrentPositionInicio => currentPositionInicio;
  LatLng get getCurrentPositionEntregaOrden => currentPositionEntregaOrden;
  LatLng get getCurrentPositionFin => currentPositionFin;

  bool get currentOrdenRapidaSimple => _paginaOrdenRapida;

  fetchAllOrdenesPendientes() async {
    sharedPreferences = await SharedPreferences.getInstance();
    //var idUser = sharedPreferences.getString("idUser");
    var idUser = sharedPreferences.getString("idUser");
    OrdenFacturaModelList ordenModel = await _repository.fetchOrdenes(idUser);
    _ordenFetcher.sink.add(ordenModel);
  }

  deleteMyDirection(String id) async {
    bool deleteMiDireccion = await _repository.deleteMyDirection(id);
    _miDireccionDelete.sink.add(deleteMiDireccion);
  }

  currentPosition(LatLng currentPosition2) async {
    currentPositionObj = currentPosition2;
    print(
        "viendo en currentPosition ---> llega bien la coordenada --< $currentPosition2");
    _myCurrentPosition.sink.add(currentPositionObj);
  }

  currentPositionParseDouble(String currentPosition2) async {
    List<String> position = currentPosition2.split(",");
    double lat = double.parse(position[0].trim());
    double lng = double.parse(position[1].trim());
    currentPositionObj = LatLng(lat, lng);
    print(
        "viendo en currentPosition ---> llega bien la coordenada --< $currentPosition2");
    _myCurrentPosition.sink.add(currentPositionObj);
  }

  currentPositionInicioFin() async {
    currentPositionEntregaOrden = currentPositionObj;
    currentPositionInicio == null
        ? currentPositionInicio = currentPositionObj
        : currentPositionFin = currentPositionObj;
    _positionEntregaOrden.sink.add(currentPositionEntregaOrden);
    _positionInicio.sink.add(currentPositionInicio);
    _positionFin.sink.add(currentPositionFin);
  }

  paginaOrdenRapida(bool paginaOrdenRapida) async {
    currentPositionEntregaOrden = null;
    currentPositionInicio = null;
    currentPositionFin = null;
    _paginaOrdenRapida = paginaOrdenRapida;
    _positionEntregaOrden.sink.add(currentPositionEntregaOrden);
    _positionInicio.sink.add(currentPositionInicio);
    _positionFin.sink.add(currentPositionFin);
    print("ingreso a paginaOrdenRapida y su valor es --> $_paginaOrdenRapida ");
  }

  dispose() {
    _ordenFetcher.close();
    _miDireccionDelete.close();
    _miDireccionCreate.close();
    _myCurrentPosition.close();
    _positionEntregaOrden.close();
    _positionInicio.close();
    _positionFin.close();
  }
}

final ordenesFactura = OrdenesFactura();
