import 'package:rioexpresscliente/bloc/bloc_provider.dart';
import 'package:rioexpresscliente/models/negocio.model.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rioexpresscliente/models/producto.model.dart';
import 'package:rioexpresscliente/resources/repository.negocio.dart';

class ProductoBloc extends BlocBase {
  final _repository = RepositoryNegocio();
  final _productoFetcher = new PublishSubject<ProductoModelList>();
  final _productoCarFetcher = new PublishSubject<List<ProductoModel>>();
  NegocioModel negocioWithServices;
  //final _negocioFetcherType = new PublishSubject<String>();

  Stream<ProductoModelList> get allProductos => _productoFetcher.stream;
  Stream get allProductosCar => _productoCarFetcher.stream;
  NegocioModel get allProductoTipoNegocio => negocioWithServices;
  String tipoNegocioLocal;

  fetchAllProductoByNegocio(String idNegocio) async {
    ProductoModelList productoModelList =
        await _repository.fetchAllProductoByNegocio(negocioWithServices.id);

    _productoFetcher.sink.add(productoModelList);
  }

  switchNegocio(NegocioModel tipoNegocio) {
    negocioWithServices = tipoNegocio;
    //_negocioFetcherType.sink.add(tipoNegocio);
  }

  dispose() {
    _productoFetcher.close();
    _productoCarFetcher.close();
    //_negocioFetcherType.close();
  }
}

final productoBloc = ProductoBloc();
