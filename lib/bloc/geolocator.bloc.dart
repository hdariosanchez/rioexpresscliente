/*
import 'package:geolocator/geolocator.dart';

final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
Position _currentPosition;
String _currentAddress;
double _distanciaMetros;
static IpConfig local = new IpConfig("192.168.1.15", "5000");

void checkPermission() {
  geolocator.checkGeolocationPermissionStatus().then((status) {
    print('status: $status');
  });
  geolocator
      .checkGeolocationPermissionStatus(
      locationPermission: GeolocationPermission.locationAlways)
      .then((status) {
    print('always status: $status');
  });
  geolocator.checkGeolocationPermissionStatus(
      locationPermission: GeolocationPermission.locationWhenInUse)
    ..then((status) {
      print('whenInUse status: $status');
    });
}

Future<Position> _getLocation() async {
  var currentLocation;
  try {
    currentLocation = await geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);
  } catch (e) {
    currentLocation = null;
  }
  return currentLocation;
}

Future<double> _getDistanceInMeters(double latitudStart, double longitudStart,
    double latitudEnd, double longitudeEnd) async {
  try {
    _distanciaMetros = await Geolocator().distanceBetween(
        latitudStart, longitudStart, latitudEnd, longitudeEnd);
  } catch (e) {
    _distanciaMetros = 0.0;
  }
  return _distanciaMetros;
}



_getCurrentLocation(double latitudNegocio, double longitudNegocio) async {
  geolocator
      .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
      .then((Position position) {
    setState(() {
      _currentPosition = position;
    });

    _getAddressFromLatLng(latitudNegocio, longitudNegocio,
        _currentPosition.latitude, _currentPosition.longitude);
  }).catchError((e) {
    print(e);
  });
}

_getAddressFromLatLng(double latitudNegocio, double longitudNegocio,
    double latitudNegocioEnd, double longitudNegocioEnd) async {
  try {
    List<Placemark> p = await geolocator.placemarkFromCoordinates(
        _currentPosition.latitude, _currentPosition.longitude);
    double distanceInMeters = await Geolocator().distanceBetween(
        latitudNegocio,
        longitudNegocio,
        latitudNegocioEnd,
        longitudNegocioEnd);

    Placemark place = p[0];

    setState(() {
      _distanciaMetros = distanceInMeters;
      _currentAddress = place.locality;
    });
  } catch (e) {
    print(e);
  }
}
*/