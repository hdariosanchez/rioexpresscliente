import 'package:rxdart/rxdart.dart';
import 'package:rioexpresscliente/models/negocio.model.dart';
import 'package:rioexpresscliente/resources/repository.negocio.dart';

class NegocioBloc {
  final _repository = RepositoryNegocio();
  final _negocioFetcher = new PublishSubject<NegocioModelList>();

  String tipoNegocioLocal;

  Stream<NegocioModelList> get allNegocio => _negocioFetcher.stream;

  fetchAllNegocio(String tipoNegocio) async {
    NegocioModelList negocioModelList =
        await _repository.fetchAllNegocio(tipoNegocioLocal);
    _negocioFetcher.sink.add(negocioModelList);
  }

  asignNegocioTipo(String tipoNegocio) {
    tipoNegocioLocal = tipoNegocio;
  }

  dispose() {
    _negocioFetcher.close();
  }
}

final negocioBloc = NegocioBloc();
