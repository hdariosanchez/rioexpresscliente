import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rioexpresscliente/models/direccion.model.dart';
import 'package:rioexpresscliente/resources/repository.midireccion.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DireccionBloc {
  final _repository = RepositoryMiDireccion();
  final _miDireccionFetcher = new PublishSubject<MiDireccionModelList>();
  final _miDireccionDelete = new PublishSubject<bool>();
  final _miDireccionCreate = new PublishSubject<bool>();
  final _myCurrentPosition = new PublishSubject<LatLng>();
  final _positionInicio = new PublishSubject<LatLng>();
  final _positionEntregaOrden = new PublishSubject<LatLng>();
  final _positionFin = new PublishSubject<LatLng>();
  LatLng currentPositionObj;
  LatLng currentPositionInicio = null;
  LatLng currentPositionEntregaOrden = null;
  LatLng currentPositionFin = null;
  bool _paginaOrdenRapida;

  SharedPreferences sharedPreferences;

  Stream<MiDireccionModelList> get allMyDirections =>
      _miDireccionFetcher.stream;

  Stream<bool> get deleteMyDirectionBool => _miDireccionDelete.stream;

  Stream<bool> get createDirectionBool => _miDireccionCreate.stream;

  Stream<LatLng> get currentPositionLatLng => _myCurrentPosition.stream;
  Stream<LatLng> get positionInicio => _positionInicio.stream;
  Stream<LatLng> get positionEntregaOrden => _positionEntregaOrden.stream;
  Stream<LatLng> get positionFin => _positionFin.stream;
  LatLng get currentPositionLatLngSimple => currentPositionObj;
  LatLng get getCurrentPositionInicio => currentPositionInicio;
  LatLng get getCurrentPositionEntregaOrden => currentPositionEntregaOrden;
  LatLng get getCurrentPositionFin => currentPositionFin;

  bool get currentOrdenRapidaSimple => _paginaOrdenRapida;

  fetchAllMyDirections() async {
    sharedPreferences = await SharedPreferences.getInstance();
    var idUser = sharedPreferences.getString("idUser");
    MiDireccionModelList miDireccionModel =
        await _repository.fetchMyDirection(idUser);
    _miDireccionFetcher.sink.add(miDireccionModel);
  }

  Future<MiDireccionModelList> fetchAllMyDirectionsForDropDow() async {
    sharedPreferences = await SharedPreferences.getInstance();
    var idUser = sharedPreferences.getString("idUser");
    MiDireccionModelList miDireccionModel =
        await _repository.fetchMyDirection(idUser);
    return miDireccionModel;
  }

  fetchAllMyDirectionsForDropDow2() async {
    sharedPreferences = await SharedPreferences.getInstance();
    var idUser = sharedPreferences.getString("idUser");
    MiDireccionModelList miDireccionModel =
        await _repository.fetchMyDirection(idUser);
    _miDireccionFetcher.sink.add(miDireccionModel);
  }

  deleteMyDirection(String id) async {
    bool deleteMiDireccion = await _repository.deleteMyDirection(id);
    _miDireccionDelete.sink.add(deleteMiDireccion);
  }

  createDireccion(MiDireccion miDireccion) async {
    bool cDirectionBool = await _repository.createMyDirection(miDireccion);
    _miDireccionCreate.sink.add(cDirectionBool);
  }

  currentPosition(LatLng currentPosition2) async {
    currentPositionObj = currentPosition2;
    print(
        "viendo en currentPosition ---> llega bien la coordenada --< $currentPosition2");
    _myCurrentPosition.sink.add(currentPositionObj);
  }

  currentPositionParseDouble(String currentPosition2) async {
    List<String> position = currentPosition2.split(",");
    double lat = double.parse(position[0].trim());
    double lng = double.parse(position[1].trim());
    currentPositionObj = LatLng(lat, lng);
    print(
        "viendo en currentPosition ---> llega bien la coordenada --< $currentPosition2");
    _myCurrentPosition.sink.add(currentPositionObj);
  }

  currentPositionInicioFin() async {
    currentPositionEntregaOrden = currentPositionObj;
    currentPositionInicio == null
        ? currentPositionInicio = currentPositionObj
        : currentPositionFin = currentPositionObj;
    _positionEntregaOrden.sink.add(currentPositionEntregaOrden);
    _positionInicio.sink.add(currentPositionInicio);
    _positionFin.sink.add(currentPositionFin);
  }

  paginaOrdenRapida(bool paginaOrdenRapida) async {
    currentPositionEntregaOrden = null;
    currentPositionInicio = null;
    currentPositionFin = null;
    _paginaOrdenRapida = paginaOrdenRapida;
    _positionEntregaOrden.sink.add(currentPositionEntregaOrden);
    _positionInicio.sink.add(currentPositionInicio);
    _positionFin.sink.add(currentPositionFin);
    print("ingreso a paginaOrdenRapida y su valor es --> $_paginaOrdenRapida ");
  }

  dispose() {
    _miDireccionFetcher.close();
    _miDireccionDelete.close();
    _miDireccionCreate.close();
    _myCurrentPosition.close();
    _positionEntregaOrden.close();
    _positionInicio.close();
    _positionFin.close();
  }
}

final direccionBloc = DireccionBloc();
