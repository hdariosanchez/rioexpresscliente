import 'dart:convert';

class Factura {
  String _id;
  String _rucCedula;
  String _nombreApellido;
  String _celular;
  String _celularAdicional;
  String _direccionEntrega;
  String _observaciones;
  String _idOrden;
  String _idUser;
  String _subTotal;
  String _iva;
  String _total;
  double _latitud;
  double _longitud;

  Factura();

  Factura.fromJson(Map<String, dynamic> parsedJson) {
    _id = parsedJson["_id"];
    _rucCedula = parsedJson["rucCedula"];
    _nombreApellido = parsedJson["nombreApellido"];
    _celular = parsedJson["celular"];
    _celularAdicional = parsedJson["celularAdicional"];
    _direccionEntrega = parsedJson["direccionEntrega"];
    _observaciones = parsedJson["observaciones"];
    _idOrden = parsedJson["idOrden"];
    _idUser = parsedJson["idUser"];
    _subTotal = parsedJson["subTotal"];
    _iva = parsedJson["iva"];
    _total = parsedJson["total"];
    _latitud = parsedJson["latitud"];
    _longitud = parsedJson["longitud"];
/*
    List<ListProductFacturado> temp = [];
    for (int i = 0;
        i < parsedJson['ordenProducto']['arrayProducto'].length;
        i++) {
      ListProductFacturado productosOrdenItem =
          ListProductFacturado(parsedJson['ordenProducto']['arrayProducto'][i]);
      temp.add(productosOrdenItem);
    }
    */
  }

  String get id => _id;
  String get rucCedula => _rucCedula;
  String get nombreApellido => _nombreApellido;
  String get celular => _celular;
  String get celularAdicional => _celularAdicional;
  String get direccionEntrega => _direccionEntrega;
  String get observaciones => _observaciones;
  String get idOrden => _idOrden;
  String get idUser => _idUser;
  String get subTotal => _subTotal;
  String get iva => _iva;
  String get total => _total;
  double get latitud => _latitud;
  double get longitud => _longitud;

  set id(String value) => _id = value;
  set rucCedula(String value) => _rucCedula = value;
  set nombreApellido(String value) => _nombreApellido = value;
  set celular(String value) => _celular = value;
  set celularAdicional(String value) => _celularAdicional = value;
  set direccionEntrega(String value) => _direccionEntrega = value;
  set observaciones(String value) => _observaciones = value;
  set idOrden(String value) => _idOrden = value;
  set idUser(String value) => _idUser = value;
  set subTotal(String value) => _subTotal = value;
  set iva(String value) => _iva = value;
  set total(String value) => _total = value;
  set latitud(double value) => _latitud = value;
  set longitud(double value) => _longitud = value;

  Map<String, dynamic> toJson() {
    return {
      "id": _id,
      "rucCedula": _rucCedula,
      "nombreApellido": _nombreApellido,
      "celular": _celular,
      "celularAdicional": _celularAdicional,
      "direccionEntrega": _direccionEntrega,
      "observaciones": _observaciones,
      "idOrden": _idOrden,
      "idUser": _idUser,
      "subTotal": _subTotal,
      "iva": _iva,
      "total": _total,
      "latitud": _latitud,
      "longitud": _longitud,
    };
  }
}

class FacturaModelList {
  List<Factura> factura = [];

  FacturaModelList.fromJson(List<dynamic> parsedJson) {
    List<Factura> facturaList = new List<Factura>();
    facturaList = parsedJson.map((item) => Factura.fromJson(item)).toList();
    factura = facturaList;
  }

  List<Factura> get results => factura;
}

String facturaToJson(Factura data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
/*
class ListProductFacturado {
  String _id;
  String _nombre;
  String _nombreCorto;
  String _descripcionProducto;
  String _precio;
  String _urlImagen;
  String _unidadMedida;
  String _estado;
  String _idCategoriaProducto;
  int _cantidadProducto;

  ListProductFacturado(productoNuevo) {
    _id = productoNuevo['_id'];
    _nombre = productoNuevo['nombre'];
    _nombreCorto = productoNuevo['nombreCorto'];
    _descripcionProducto = productoNuevo['descripcionProducto'];
    _precio = productoNuevo['precio'];
    _urlImagen = productoNuevo['urlImagen'];
    _unidadMedida = productoNuevo['unidadMedida'];
    _estado = productoNuevo['estado'];
    _idCategoriaProducto = productoNuevo['idCategoriaProducto'];
    _cantidadProducto = productoNuevo['cantidadProducto'];
  }

  set setCantidadProducto(int cantidadProducto) {
    _cantidadProducto = cantidadProducto;
    //_precio = (double.parse(_precio) * _cantidadProducto).toString();
  }

  String get id => _id;
  String get nombre => _nombre;
  String get nombreCorto => _nombreCorto;
  String get urlImagen => _urlImagen;
  String get unidadMedida => _unidadMedida;
  String get descripcionProducto => _descripcionProducto;
  String get precio => _precio;
  String get estado => _estado;
  String get idCategoriaProducto => _idCategoriaProducto;
  int get cantidadProducto => _cantidadProducto;
}
*/
