import 'package:rioexpresscliente/pages/addMiDireccion.ui.dart';
import 'dart:convert';

class MiDireccion {
  String _id;
  String _idCliente;
  String _nombre;
  double _latitud;
  double _longitud;

  MiDireccion();

  MiDireccion.fromJson(Map<String, dynamic> parsedJson) {
    _id = parsedJson["_id"];
    _idCliente = parsedJson["idCliente"];
    _nombre = parsedJson["nombre"];
    _latitud = parsedJson["localizacion"]["coordinates"][0];
    _longitud = parsedJson["localizacion"]["coordinates"][1];
  }

  String get id => _id;
  String get idCliente => _idCliente;
  String get nombre => _nombre;
  double get latitud => _latitud;
  double get longitud => _longitud;

  set id (String id) => _id = id;
  set idCliente(String idCliente) => _idCliente = idCliente;
  set nombre(String nombre) => _nombre = nombre;
  set latitud (double latitud) => _latitud = latitud;
  set longitud(double longitud) => _longitud = longitud;


  Map<String, dynamic> toJson() {
    return {"_id": id, "idCliente":_idCliente, "nombre": _nombre, "latitud":_latitud, "longitud":_longitud};
  }


}

class MiDireccionModelList {
  List<MiDireccion> miDireccion = [];

  MiDireccionModelList.fromJson(List<dynamic> parsedJson) {
    List<MiDireccion> miDireccionList = new List<MiDireccion>();
    miDireccionList =
        parsedJson.map((item) => MiDireccion.fromJson(item)).toList();
    miDireccion = miDireccionList;
  }

  List<MiDireccion> get results => miDireccion;
}

String misLugaresToJson(MiDireccion data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
