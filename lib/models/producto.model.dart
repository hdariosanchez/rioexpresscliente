import 'dart:convert';

class ProductoModel {
  String _id;
  String _nombre;
  String _nombreCorto;
  String _urlImagen;
  String _unidadMedida;
  String _estado;
  String _idCategoriaProducto;
  String _descripcionProducto;
  String _precio;
  int _stock;
  int _cantidadProducto;
  String _idCategoriaServicio;

  ProductoModel.fromJson(Map<String, dynamic> parsedJson) {
    //print(parsedJson['result'].length);
    _id = parsedJson['_id'];
    _nombre = parsedJson['nombre'];
    _nombreCorto = parsedJson['nombreCorto'];
    _urlImagen = parsedJson['urlImagen'];
    _unidadMedida = parsedJson['unidadMedida'];
    _estado = parsedJson['estado'];
    _cantidadProducto = 1;
    _idCategoriaProducto = parsedJson['idCategoriaProducto'];
    _descripcionProducto =
        parsedJson['descripcionProducto']['descripcionProducto'];
    _precio = parsedJson['descripcionProducto']['precio'];
    _stock = parsedJson['lineaServicioProductoList']['stock'];
    _idCategoriaServicio =
        parsedJson['lineaServicioProductoList']['idCategoriaServicio'];
  }

  String get id => _id;
  String get nombre => _nombre;
  String get nombreCorto => _nombreCorto;
  String get urlImagen => _urlImagen;
  String get unidadMedida => _unidadMedida;
  String get estado => _estado;
  String get idCategoriaProducto => _idCategoriaProducto;
  String get descripcionProducto => _descripcionProducto;
  String get precio => _precio;
  int get stock => _stock;
  int get cantidadProducto => _cantidadProducto;
  String get idCategoriaServicio => _idCategoriaServicio;

  Map<String, dynamic> toJson() {
    return {
      "_id": id,
      "nombre": nombre,
      "nombreCorto": nombreCorto,
      "descripcionProducto": descripcionProducto,
      "precio": precio,
      "urlImagen": urlImagen,
      "unidadMedida": unidadMedida,
      "estado": estado,
      "idCategoriaProducto": idCategoriaProducto,
      "cantidadProducto": 1
    };
  }
}

class ProductoModelList {
  List<ProductoModel> productos = [];

  ProductoModelList.fromJson(List<dynamic> parsedJson) {
    List<ProductoModel> productosList = new List<ProductoModel>();
    productosList =
        parsedJson.map((item) => ProductoModel.fromJson(item)).toList();
    productos = productosList;
  }

  List<ProductoModel> get results => productos;
}

String productoToJson(ProductoModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
