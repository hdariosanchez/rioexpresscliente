import 'dart:convert';

import 'package:rioexpresscliente/models/producto.model.dart';

class OrdenModel {
  String _id;
  String _tipoOrden;
  String _idCliente;
  String _estado;
  List<ListProductCar> _arrayProducto = [];
  List<ProductoModel> _arrayProductoCreate = [];

  OrdenModel();

  OrdenModel.fromJson(Map<String, dynamic> parsedJson) {
    _id = parsedJson['_id'];
    _tipoOrden = parsedJson['tipoOrden'];
    _idCliente = parsedJson['idCliente'];
    _estado = parsedJson['estado'];

    List<ListProductCar> temp = [];
    for (int i = 0; i < parsedJson['arrayProducto'].length; i++) {
      ListProductCar productosOrdenItem =
          ListProductCar(parsedJson['arrayProducto'][i], true);
      temp.add(productosOrdenItem);
    }
    _arrayProducto = temp;
  }

  List<ListProductCar> get arrayProducto => _arrayProducto;
  String get id => _id;
  String get tipoOrden => _tipoOrden;
  String get idCliente => _idCliente;
  String get estado => _estado;

  set id(String id) => _id = id;
  set idCliente(String idCliente) => _idCliente = idCliente;
  set tipoOrden(String tipoOrden) => _tipoOrden = tipoOrden;
  set estado(String estado) => _estado = estado;
  set arrayProductoCreate(ProductoModel arrayProducto) {
    List<ProductoModel> temp = [];
    temp.add(arrayProducto);
    _arrayProductoCreate = temp;
  }

  Map<String, dynamic> toJson() {
    return {
      "_id": id,
      "idCliente": _idCliente,
      "tipoOrden": _tipoOrden,
      "estado": _estado,
      "arrayProducto": [
        {
          "_id": _arrayProductoCreate[0].id,
          "nombre": _arrayProductoCreate[0].nombre,
          "nombreCorto": _arrayProductoCreate[0].nombreCorto,
          "descripcionProducto": _arrayProductoCreate[0].descripcionProducto,
          "precio": _arrayProductoCreate[0].precio,
          "urlImagen": _arrayProductoCreate[0].urlImagen,
          "unidadMedida": _arrayProductoCreate[0].unidadMedida,
          "estado": _arrayProductoCreate[0].estado,
          "idCategoriaProducto": _arrayProductoCreate[0].idCategoriaProducto,
          "cantidadProducto": 1
        }
      ],
    };
  }
}

String ordenToJson(OrdenModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}

class OrdenModelList {
  List<OrdenModel> orden = [];

  OrdenModelList.fromJson(List<dynamic> parsedJson) {
    List<OrdenModel> ordenList = new List<OrdenModel>();
    ordenList = parsedJson.map((item) => OrdenModel.fromJson(item)).toList();
    orden = ordenList;
  }

  List<OrdenModel> get results => orden;
}

class ListProductCar {
  String _id;
  String _nombre;
  String _nombreCorto;
  String _descripcionProducto;
  String _precio;
  String _urlImagen;
  String _unidadMedida;
  String _estado;
  String _idCategoriaProducto;
  int _cantidadProducto;

  ListProductCar(productoNuevo, bandera) {
    if (bandera) {
      _id = productoNuevo['_id'];
      _nombre = productoNuevo['nombre'];
      _nombreCorto = productoNuevo['nombreCorto'];
      _descripcionProducto = productoNuevo['descripcionProducto'];
      _precio = productoNuevo['precio'];
      _urlImagen = productoNuevo['urlImagen'];
      _unidadMedida = productoNuevo['unidadMedida'];
      _estado = productoNuevo['estado'];
      _idCategoriaProducto = productoNuevo['idCategoriaProducto'];
      _cantidadProducto = productoNuevo['cantidadProducto'];
    } else {
      _id = productoNuevo.id;
      _nombre = productoNuevo.nombre;
      _nombreCorto = productoNuevo.nombreCorto;
      _descripcionProducto = productoNuevo.descripcionProducto;
      _precio = productoNuevo.precio;
      _urlImagen = productoNuevo.urlImagen;
      _unidadMedida = productoNuevo.unidadMedida;
      _estado = productoNuevo.estado;
      _idCategoriaProducto = productoNuevo.idCategoriaProducto;
      _cantidadProducto = productoNuevo.cantidadProducto;
    }
  }

  set setCantidadProducto(int cantidadProducto) {
    _cantidadProducto = cantidadProducto;
    //_precio = (double.parse(_precio) * _cantidadProducto).toString();
  }

  String get id => _id;
  String get nombre => _nombre;
  String get nombreCorto => _nombreCorto;
  String get urlImagen => _urlImagen;
  String get unidadMedida => _unidadMedida;
  String get descripcionProducto => _descripcionProducto;
  String get precio => _precio;
  String get estado => _estado;
  String get idCategoriaProducto => _idCategoriaProducto;
  int get cantidadProducto => _cantidadProducto;
}
