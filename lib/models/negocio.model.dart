import 'package:rioexpresscliente/models/producto.model.dart';

class NegocioModel {
  String _id;
  String _rucNegocio;
  String _nombreComercial;
  String _nombreCorto;
  String _descripcionLocal;
  String _urlImagen;
  String _email;
  String _celular;
  String _convencional;
  String _callePrincipal;
  String _calleSecundaria;
  String _referencia;
  String _estado;
  String _idCategoriaNegocio;
  String _idCanton;
  String _createdAt;
  String _updatedAt;

  double _latitude;
  double _longitude;

  double _latitud;
  double _longitud;

  List<_ServicioNegocio> _servicioNegocio = [];

  NegocioModel.fromJson(Map<String, dynamic> parsedJson) {
    //print(parsedJson['result'].length);
    _id = parsedJson['_id'];
    _rucNegocio = parsedJson['rucNegocio'];
    _nombreComercial = parsedJson['nombreComercial'];
    _nombreCorto = parsedJson['nombreCorto'];
    _descripcionLocal = parsedJson['descripcionLocal'];
    _urlImagen = parsedJson['urlImagen'];
    _email = parsedJson['email'];
    _celular = parsedJson['celular'];
    _convencional = parsedJson['convencional'];
    _callePrincipal = parsedJson['callePrincipal'];
    _calleSecundaria = parsedJson['calleSecundaria'];
    _referencia = parsedJson['referencia'];
    _estado = parsedJson['estado'];
    _idCategoriaNegocio = parsedJson['idCategoriaNegocio'];
    _idCanton = parsedJson['idCanton'];
    _createdAt = parsedJson['createdAt'];
    _updatedAt = parsedJson['updatedAt'];
    _latitud = parsedJson['localizacion']['coordinates'][0];
    _longitud = parsedJson['localizacion']['coordinates'][1];
    _latitude = 0.0;
    _longitude = 0.0;
    List<_ServicioNegocio> temp = [];
    for (int i = 0; i < parsedJson['servicioNegocio'].length; i++) {
      _ServicioNegocio servicioNegocioItem =
          _ServicioNegocio(parsedJson['servicioNegocio'][i]);
      temp.add(servicioNegocioItem);
    }
    _servicioNegocio = temp;
  }

  List<_ServicioNegocio> get servicioNegocio => _servicioNegocio;
  String get id => _id;
  String get rucNegocio => _rucNegocio;
  String get nombreComercial => _nombreComercial;
  String get nombreCorto => _nombreCorto;
  String get descripcionLocal => _descripcionLocal;
  String get urlImagen => _urlImagen;
  String get email => _email;
  String get celular => _celular;
  String get convencional => _convencional;
  String get callePrincipal => _callePrincipal;
  String get calleSecundaria => _calleSecundaria;
  String get referencia => _referencia;
  String get estado => _estado;
  String get idCategoriaNegocio => _idCategoriaNegocio;
  String get idCanton => _idCanton;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;

  double get latitud => _latitud;
  double get longitud => _longitud;

  double get latitude => _latitude;
  double get longitude => _longitude;

  set latitude(double latitude) {
    this._latitude = latitude;
  }

  set longitude(double longitude) {
    this._longitude = longitude;
  }

  /*
  Future<double> _getDistanceInMeters(
      double latitudStart, double longitudStart) async {
    try {
      _distanciaMetros = await Geolocator().distanceBetween(
          latitudStart, longitudStart, this._latitud, this.longitud);
      this._distanciaCalculada = _distanciaMetros;
    } catch (e) {
      _distanciaMetros = 0.0;
    }
    return _distanciaMetros;
  }
  */
}

class NegocioModelList {
  List<NegocioModel> negocios = [];

  NegocioModelList.fromJson(List<dynamic> parsedJson) {
    List<NegocioModel> negociosList = new List<NegocioModel>();
    negociosList =
        parsedJson.map((item) => NegocioModel.fromJson(item)).toList();
    negocios = negociosList;
  }

  List<NegocioModel> get results => negocios;
}

class _ServicioNegocio {
  String _id;
  String _nombre;
  String _descripcion;
  String _idNegocio;
  String _estado;
  List<ProductoModel> _productos = [];

  _ServicioNegocio(servicioNegocio) {
    _id = servicioNegocio['_id'];
    _nombre = servicioNegocio['nombre'];
    _descripcion = servicioNegocio['descripcion'];
    _idNegocio = servicioNegocio['idNegocio'];
    _estado = servicioNegocio['estado'];
  }

  String get id => _id;
  String get nombre => _nombre;
  String get descripcion => _descripcion;
  String get idNegocio => _idNegocio;
  String get estado => _estado;
  List<ProductoModel> get results => _productos;
}
