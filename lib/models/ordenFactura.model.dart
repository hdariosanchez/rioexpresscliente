import 'dart:convert';

class OrdenFacturaModel {
  String _id;
  double _latitud;
  double _longitud;
  String _rucCedula;
  String _celular;
  String _celularAdicional;
  String _direccionEntrega;
  String _observaciones;
  String _idOrden;

  String _subTotal;
  String _iva;
  String _total;
  //double _calcDistance;
  String _tipoOrden;
  String _idCliente;
  String _estado;
  List<ListProductFacturado> _arrayProducto = [];

  OrdenFacturaModel();

  OrdenFacturaModel.fromJson(Map<String, dynamic> parsedJson) {
    _id = parsedJson['_id'];
    _latitud = parsedJson['localizacion']['coordinates'][0];
    _longitud = parsedJson['localizacion']['coordinates'][1];
    _rucCedula = parsedJson['rucCedula'];
    _celular = parsedJson['celular'];
    _celularAdicional = parsedJson['celularAdicional'];
    _direccionEntrega = parsedJson['direccionEntrega'];
    _observaciones = parsedJson['observaciones'];
    _idOrden = parsedJson['idOrden'];
    _subTotal = parsedJson['subTotal'];
    _iva = parsedJson['iva'];
    _total = parsedJson['total'];
    //_calcDistance = double.parse(parsedJson['calcDistance'].toStringAsFixed(2));
    _tipoOrden = parsedJson['tipoOrden'];
    _idCliente = parsedJson['ordenProducto']['idCliente'];
    _estado = parsedJson['estado'];

    List<ListProductFacturado> temp = [];
    for (int i = 0;
        i < parsedJson['ordenProducto']['arrayProducto'].length;
        i++) {
      ListProductFacturado productosOrdenItem =
          ListProductFacturado(parsedJson['ordenProducto']['arrayProducto'][i]);
      temp.add(productosOrdenItem);
    }
    _arrayProducto = temp;
  }

  List<ListProductFacturado> get arrayProducto => _arrayProducto;
  String get id => _id;
  double get latitud => _latitud;
  double get longitud => _longitud;
  String get rucCedula => _rucCedula;
  String get celular => _celular;
  String get celularAdicional => _celularAdicional;
  String get direccionEntrega => _direccionEntrega;
  String get observaciones => _observaciones;
  String get idOrden => _idOrden;
  String get subTotal => _subTotal;
  String get iva => _iva;
  String get total => _total;
  //double get calcDistance => _calcDistance;
  String get tipoOrden => _tipoOrden;
  String get idCliente => _idCliente;
  String get estado => _estado;
/*
  set id(String id) => _id = id;
  set idCliente(String idCliente) => _idCliente = idCliente;
  set tipoOrden(String tipoOrden) => _tipoOrden = tipoOrden;
  set estado(String estado) => _estado = estado;
*/
/*
  Map<String, dynamic> toJson() {
    return {
      "_id": id,
      "idCliente": _idCliente,
      "tipoOrden": _tipoOrden,
      "estado": _estado,
      "arrayProducto": [
        {
          "_id": _arrayProducto[0].id,
          "nombre": _arrayProducto[0].nombre,
          "nombreCorto": _arrayProducto[0].nombreCorto,
          "descripcionProducto": _arrayProducto[0].descripcionProducto,
          "precio": _arrayProducto[0].precio,
          "urlImagen": _arrayProducto[0].urlImagen,
          "unidadMedida": _arrayProducto[0].unidadMedida,
          "estado": _arrayProducto[0].estado,
          "idCategoriaProducto": _arrayProducto[0].idCategoriaProducto,
          "cantidadProducto": 1
        }
      ],
    };
  }
*/
}

/*
String ordenToJson(OrdenFacturaModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
*/

class OrdenFacturaModelList {
  List<OrdenFacturaModel> orden = [];

  OrdenFacturaModelList.fromJson(List<dynamic> parsedJson) {
    List<OrdenFacturaModel> ordenList = new List<OrdenFacturaModel>();
    ordenList =
        parsedJson.map((item) => OrdenFacturaModel.fromJson(item)).toList();
    orden = ordenList;
  }

  List<OrdenFacturaModel> get results => orden;
}

class ListProductFacturado {
  String _id;
  String _nombre;
  String _nombreCorto;
  String _descripcionProducto;
  String _precio;
  String _urlImagen;
  String _unidadMedida;
  String _estado;
  String _idCategoriaProducto;
  int _cantidadProducto;

  ListProductFacturado(productoNuevo) {
    _id = productoNuevo['_id'];
    _nombre = productoNuevo['nombre'];
    _nombreCorto = productoNuevo['nombreCorto'];
    _descripcionProducto = productoNuevo['descripcionProducto'];
    _precio = productoNuevo['precio'];
    _urlImagen = productoNuevo['urlImagen'];
    _unidadMedida = productoNuevo['unidadMedida'];
    _estado = productoNuevo['estado'];
    _idCategoriaProducto = productoNuevo['idCategoriaProducto'];
    _cantidadProducto = productoNuevo['cantidadProducto'];
  }

  set setCantidadProducto(int cantidadProducto) {
    _cantidadProducto = cantidadProducto;
    //_precio = (double.parse(_precio) * _cantidadProducto).toString();
  }

  String get id => _id;
  String get nombre => _nombre;
  String get nombreCorto => _nombreCorto;
  String get urlImagen => _urlImagen;
  String get unidadMedida => _unidadMedida;
  String get descripcionProducto => _descripcionProducto;
  String get precio => _precio;
  String get estado => _estado;
  String get idCategoriaProducto => _idCategoriaProducto;
  int get cantidadProducto => _cantidadProducto;
}
