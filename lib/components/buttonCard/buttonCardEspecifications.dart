/*
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:findi/geoLocalization.dart';
import 'package:findi/model/userLocation.dart';
import 'package:toast/toast.dart';

import '../../urlLauncher.dart';

class ButtonCardEspecifications extends StatelessWidget {
  final int codigoIcono;
  final int codigoColor;
  final int codigoProceso;
  final double openMapsLat;
  final double openMapsLong;

  ButtonCardEspecifications(this.codigoColor, this.codigoIcono, this.codigoProceso, this.openMapsLat, this.openMapsLong);

  IconData _chooseIcons(codigoIcono) {
    switch (codigoIcono) {
      case 1:
        return Icons.favorite;
      case 2:
        return Icons.comment;
      case 3:
        return Icons.share;
        break;
      default:
        return Icons.markunread_mailbox;
    }
  }

  MaterialAccentColor _chooseColor(codigoColor) {
    switch (codigoColor) {
      case 1:
        return Colors.redAccent;
      case 2:
        return Colors.blueAccent;
      case 3:
        return Colors.orangeAccent;
        break;
      default:
        return Colors.greenAccent;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Expanded(
      child: new FlatButton.icon(
        // Un icono puede recibir muchos atributos, aqui solo usaremos icono, tamaño y color
        icon: Icon(
          _chooseIcons(codigoIcono),
          size: 18.0,
          color: _chooseColor(codigoColor),
        ),
        label: const Text(''),
        // Esto mostrara 'Me encanta' por la terminal
        onPressed: () async {
          switch (codigoProceso){
            // ignore: missing_return
            case 1:
              print('Me encanta');
              break;
            case 2:
              {
                UserLocation userLocation =
                    await LocationService().getLocation();
                Toast.show(
                    "Desde model -->" + userLocation.longitude.toString(),
                    context,
                    duration: Toast.LENGTH_LONG,
                    gravity: Toast.BOTTOM);
              }
              break;
            case 3:
              OpenUtils.openMap(openMapsLat, openMapsLong);
              break;
            default:
              return 0;
          }
        },
      ),
    );
  }
}

*/