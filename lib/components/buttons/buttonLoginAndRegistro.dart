
/*
import 'package:findi/animations/loading.dart';
import 'package:findi/views/newUserRegister.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../../urlLauncher.dart';

class ButtonLoginAndRegister extends StatelessWidget {
  final String title;
  final List<Color> gradient;
  final bool isEndIconVisible;
  final bool isLocalOrFacebook;

  ButtonLoginAndRegister(this.title, this.gradient,
      this.isEndIconVisible, this.isLocalOrFacebook);

  IconData _chooseIcons(codigoIcono) {
    switch (codigoIcono) {
      case 1:
        return Icons.favorite;
      case 2:
        return Icons.comment;
      case 3:
        return Icons.share;
        break;
      default:
        return Icons.markunread_mailbox;
    }
  }

  MaterialAccentColor _chooseColor(codigoColor) {
    switch (codigoColor) {
      case 1:
        return Colors.redAccent;
      case 2:
        return Colors.blueAccent;
      case 3:
        return Colors.orangeAccent;
        break;
      default:
        return Colors.greenAccent;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: InkWell(
        // When the user taps the button, show a snackbar.
        onTap: () {
          Loader();
          isLocalOrFacebook
              ? Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => newUserRegister()))
              : Toast.show("Iniciando con Facebook", context,
              duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        },
        child: Container(
          alignment: Alignment.center,
          width: MediaQuery.of(context).size.width / 1.7,
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            gradient: LinearGradient(
                colors: gradient,
                begin: Alignment.topLeft,
                end: Alignment.bottomRight),
          ),
          child: Text(title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w500)),
          padding: EdgeInsets.only(top: 16, bottom: 16),
        ),
      ),
    );
  }
}
*/