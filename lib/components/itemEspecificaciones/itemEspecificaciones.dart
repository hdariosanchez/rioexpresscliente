import 'package:flutter/material.dart';

class ItemEspecificaciones extends StatelessWidget {
  final int codigoColor;
  final int codigoIcono;
  final String descripcion;

  ItemEspecificaciones(this.codigoColor,this.codigoIcono, this.descripcion);

  IconData _chooseIcons(codigoIcono) {
    switch (codigoIcono) {
      case 1:
        return Icons.supervised_user_circle;
      case 2:
        return Icons.mouse;
      case 3:
        return Icons.arrow_forward;
        break;
      default:
        return Icons.zoom_out_map;
    }
  }
  MaterialColor _chooseColor(codigoColor) {
    switch (codigoColor) {
      case 1:
        return Colors.pink;
      case 2:
        return Colors.blue;
      case 3:
        return Colors.orange;
        break;
      default:
        return Colors.green;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(0),
          child: Icon(
            _chooseIcons(codigoIcono),
            color: _chooseColor(codigoColor),
            size: 15.0,
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(right: 5, left: 5),
            child: Text(
              descripcion,
              style: const TextStyle(
                color: Colors.black87,
                fontSize: 12,
              ),
              textAlign: TextAlign.justify,
              textDirection: TextDirection.rtl,
            ),
          ),
        ),
      ],
    );
  }
}
