
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class TitleEspecification extends StatelessWidget {
  final int codigoProceso;
  final String title;

  TitleEspecification(this.codigoProceso, this.title);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      color: Colors.white,
      elevation: 2.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(2.0),
      ),
      child: Padding(
        padding: EdgeInsets.all(1),
        child: Text(
          title,
          style: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.bold,
              //fontStyle: FontStyle.italic,
              color: Colors.black),
        ),
      ),
    );
  }
}

