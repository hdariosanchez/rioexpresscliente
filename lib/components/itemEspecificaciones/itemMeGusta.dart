import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ItemMeGusta extends StatelessWidget {
  //final bool localFisico;
  final bool codigoColorOpen;
  final String codigoProducto;

  ItemMeGusta(this.codigoColorOpen, this.codigoProducto);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
        new Container(
          padding: EdgeInsets.all(1),
          decoration: new BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(6),
          ),
          constraints: BoxConstraints(
            minWidth: 13,
            minHeight: 13,
          ),
          child: Icon(
            Icons.favorite,
            size: 13.0,
            color: !codigoColorOpen ? Colors.green : Colors.red,
          ),
        );
  }
}
