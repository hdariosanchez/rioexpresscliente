import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ItemOpenClosed extends StatelessWidget {
  final bool localFisico;
  final bool codigoColorOpen;

  ItemOpenClosed(this.localFisico, this.codigoColorOpen);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        localFisico
            ? new Container(
                padding: EdgeInsets.all(1),
                decoration: new BoxDecoration(
                  color: codigoColorOpen ? Colors.green : Colors.red,
                  borderRadius: BorderRadius.circular(6),
                ),
                constraints: BoxConstraints(
                  minWidth: 12,
                  minHeight: 12,
                ),
                child: new Text(
                  codigoColorOpen ? "Abierto" : "Cerrado",
                  style: new TextStyle(
                    color: Colors.white,
                    fontSize: 13,
                  ),
                  textAlign: TextAlign.center,
                ),
              )
            : Padding(
                padding: EdgeInsets.all(3.0),
              )
      ],
    );
  }
}
