import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ItemPrecio extends StatelessWidget {
  //final bool localFisico;
  final bool codigoColorOpen;
  final String precioProducto;

  ItemPrecio(this.codigoColorOpen,this.precioProducto);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
        new Container(
          padding: EdgeInsets.all(1),
          decoration: new BoxDecoration(
            color: codigoColorOpen ? Colors.green : Colors.red,
            borderRadius: BorderRadius.circular(6),
          ),
          constraints: BoxConstraints(
            minWidth: 13,
            minHeight: 13,
          ),
          child: new Text(
            codigoColorOpen ? precioProducto : precioProducto,
            style: new TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 13,
            ),
            textAlign: TextAlign.center,
          ),
        );
  }
}
