import 'package:flutter/material.dart';

class InputLoginAndRegister extends StatelessWidget {
  final int codigoInputType;
  final int codigoIcono;
  final String title;
  final String hintText;
  final String valueEmpty;
  TextEditingController _controller = TextEditingController();

  InputLoginAndRegister(this.codigoInputType, this.codigoIcono, this.title,
      this.hintText, this.valueEmpty, this._controller);

  IconData _chooseIcons(codigoIcono) {
    switch (codigoIcono) {
      case 1:
        return Icons.alternate_email;
      case 2:
        return Icons.fingerprint;
      case 3:
        return Icons.perm_identity;
        break;
      case 4:
        return Icons.phone_iphone;
        break;
      case 5:
        return Icons.home;
        break;
      case 6:
        return Icons.add_comment;
        break;
      default:
        return Icons.zoom_out_map;
    }
  }

  TextInputType _chooseInputType(codigoInputType) {
    switch (codigoInputType) {
      case 1:
        return TextInputType.emailAddress;
      case 2:
        return TextInputType.text;
      case 3:
        return TextInputType.number;
        break;
      case 4:
        return TextInputType.phone;
        break;
      default:
        return TextInputType.text;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextFormField(
      controller: _controller,
      decoration: InputDecoration(
        icon: Icon(
          _chooseIcons(codigoIcono),
        ),
        hintText: hintText,
        labelText: title,
      ),
      validator: (value) {
        if (value.isEmpty) {
          return valueEmpty;
        }
      },
      keyboardType: _chooseInputType(codigoInputType),
    );
  }
}
