import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImageItemEspecification extends StatelessWidget {
  final bool codigoIcono;
  final bool codigoColor;
  final int codigoProceso;
  final String urlImage;

  ImageItemEspecification(
      this.codigoColor, this.codigoIcono, this.codigoProceso, this.urlImage);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      width: 40,
      height: 40,
      child: Stack(
        //alignment: Alignment(1, 1),
        children: [
          new Positioned(
            left: 1,
            bottom: 0,
            child: new Container(
              padding: EdgeInsets.all(0),
              decoration: new BoxDecoration(
                color: codigoColor ? Colors.green : Colors.red,
                borderRadius: BorderRadius.circular(6),
              ),
              constraints: BoxConstraints(
                minWidth: 12,
                minHeight: 12,
              ),
              child: Icon(
                codigoIcono ? Icons.check_circle_outline : Icons.clear,
                size: 10.0,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
      decoration: new BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.cyan[200],
        image: new DecorationImage(
          fit: BoxFit.cover,
          image: new NetworkImage(urlImage),
        ),
      ),
    );
  }
}

//-----------------------------------------------------------------------------------------

class ImageProduct extends StatelessWidget {
  final int codigoProceso;
  final String urlImage;

  ImageProduct(this.codigoProceso, this.urlImage);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(
            image: new NetworkImage(urlImage), fit: BoxFit.fill),
      ),
    );
  }
}
