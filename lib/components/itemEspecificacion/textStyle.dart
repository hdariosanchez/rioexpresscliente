import 'package:flutter/material.dart';

class TextStyleP extends StatelessWidget {
  final int codigoColor;
  final int codigoAlign;
  final double tamanoLetra;
  final String descripcion;

  TextStyleP(
      this.codigoColor, this.codigoAlign, this.tamanoLetra, this.descripcion);

  Color _chooseColor(codigoColor) {
    switch (codigoColor) {
      case 1:
        return Colors.pink;
        break;
      case 2:
        return Colors.white;
        break;
      case 3:
        return Colors.black54;
        break;
      case 4:
        return Colors.black;
        break;
      default:
        return Colors.green;
    }
  }

  TextAlign _chooseAlignText(codigoAlign) {
    switch (codigoAlign) {
      case 1:
        return TextAlign.center;
        break;
      case 2:
        return TextAlign.start;
        break;
      case 3:
        return TextAlign.end;
        break;
      case 4:
        return TextAlign.justify;
        break;
      case 5:
        return TextAlign.left;
        break;
      default:
        return TextAlign.right;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text(
      descripcion,
      //textAlign: TextAlign.center,
      textAlign: _chooseAlignText(codigoAlign),
      style: TextStyle(
        color: _chooseColor(codigoColor),
        fontSize: tamanoLetra,
        fontWeight: FontWeight.bold,
      ),
      textDirection: TextDirection.rtl,
    );
  }
}
