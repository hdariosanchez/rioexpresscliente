import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ItemPrecio extends StatelessWidget {
  //final bool localFisico;
  final bool codigoColorOpen;
  final String precioProducto;
  final double tamanoPrecio;

  ItemPrecio(this.codigoColorOpen, this.precioProducto, this.tamanoPrecio);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      padding: EdgeInsets.only(left: 6, right: 6),
      decoration: new BoxDecoration(
        color: codigoColorOpen ? Colors.green : Colors.red,
        borderRadius: BorderRadius.circular(6),
      ),
      constraints: BoxConstraints(
        minWidth: tamanoPrecio,
        minHeight: tamanoPrecio,
      ),
      child: new Text(
        codigoColorOpen ? precioProducto : precioProducto,
        style: new TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: tamanoPrecio,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}
