import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ItemDescriptionProduct extends StatelessWidget {
  //final bool localFisico;
  final String codigoProducto;
  final String descripcionProducto;

  ItemDescriptionProduct(this.codigoProducto, this.descripcionProducto);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Card(
      color: Colors.transparent,
      elevation: 1.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 2.0, right: 2.0),
        child: Text(
          descripcionProducto,
          style: const TextStyle(
            fontWeight: FontWeight.w300,
            color: Colors.white,
            fontSize: 12,
          ),
          textAlign: TextAlign.justify,
          //textDirection: TextDirection.rtl,
        ),
      ),
    );
  }
}
