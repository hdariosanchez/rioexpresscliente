import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonCardEspecifications extends StatelessWidget {
  final int codigoIcono;
  final int codigoColor;
  final int codigoProceso;
  final double openMapsLat;
  final double openMapsLong;

  ButtonCardEspecifications(this.codigoColor, this.codigoIcono,
      this.codigoProceso, this.openMapsLat, this.openMapsLong);

  IconData _chooseIcons(codigoIcono) {
    switch (codigoIcono) {
      case 1:
        return Icons.location_on;
      case 2:
        return Icons.comment;
      case 3:
        return Icons.share;
        break;
      default:
        return Icons.markunread_mailbox;
    }
  }

  Color _chooseColor(codigoColor) {
    switch (codigoColor) {
      case 1:
        return Colors.red;
      case 2:
        return Colors.blue;
      case 3:
        return Colors.orange;
        break;
      default:
        return Colors.white;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Expanded(
      child: new FlatButton.icon(
        // Un icono puede recibir muchos atributos, aqui solo usaremos icono, tamaño y color
        icon: Icon(
          _chooseIcons(codigoIcono),
          size: 18.0,
          color: _chooseColor(codigoColor),
        ),
        label: const Text(''),
        // Esto mostrara 'Me encanta' por la terminal
        onPressed: () async {},
      ),
    );
  }
}
