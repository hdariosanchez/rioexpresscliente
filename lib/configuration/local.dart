class IpConfig {
  String _ip;
  String _port;

  IpConfig(String ip, String port) {
    this._ip = ip;
    this._port = port;
  }

  String getIp() {
    return _ip;
  }

  String getPort() {
    return _port;
  }
}

/*
const localConfig {
  ip:"192.168.1.15",
  port:"5000",
}
export localConfig;
*/
