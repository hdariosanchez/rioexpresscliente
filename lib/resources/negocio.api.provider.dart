import 'dart:async';
import 'dart:convert';
//import 'package:flutter/foundation.dart';
import 'package:http/http.dart' show Client, Response;
import 'package:rioexpresscliente/models/negocio.model.dart';
import 'package:rioexpresscliente/models/producto.model.dart';
import 'package:rioexpresscliente/configuration/local.dart';

class NegocioApiProvider {
  Client client = Client();
  static IpConfig local = new IpConfig("192.168.1.15", "5000");
  //static IpConfig local = new IpConfig("35.247.235.167", "5000");
  final _baseUrl = "http://${local.getIp()}:${local.getPort()}/api/mongo_v1";

  Future<NegocioModelList> fetchAllNegocio(String tipoNegocio) async {
    //Future<List<NegocioModel>> fetchAllNegocio() async {
    Response response;
    try {
      response = await client.get("$_baseUrl/negocio/all/$tipoNegocio");
      if (response.statusCode == 200) {
        //return NegocioModel.fromJson(json.decode(response.body));
        return NegocioModelList.fromJson(json.decode(response.body));
        //return compute(parseData, response.body);
      } else {
        throw new Exception('Fallo cargar los negocios solicitados');
      }
    } catch (Exception) {
      print("Error de conexion con el server" + Exception.toString());
    }

    return null;
  }

  Future<ProductoModelList> fetchAllProductoByNegocio(String idNegocio) async {
    Response response;
    try {
      response =
          await client.get("$_baseUrl/producto/servicio/negocio/$idNegocio");

      if (response.statusCode == 200) {
        //return NegocioModel.fromJson(json.decode(response.body));
        return ProductoModelList.fromJson(json.decode(response.body));
        //return compute(parseData, response.body);
      } else {
        throw new Exception('Fallo cargar los negocios solicitados');
      }
    } catch (Exception) {
      print("Error de conexion con el server" + Exception.toString());
    }
    return ProductoModelList.fromJson(json.decode([
      {
        '_id': '5f07fc7378d0720a2e259f08',
        'urlImagen': '1',
        'nombreCorto': 'Lo sentimos',
        'nombre': 'Al parecer no cuentas con internet',
        'unidadMedida': '1',
        'estado': 'ACTIVO',
        'cantidadProducto': 1,
        'idCategoriaProducto': "idCategoriaProducto",
        'descripcionProducto': {
          'descripcionProducto':
              'Al parecer no cuentas con internet DESCRIPCION',
          'precio': '0.0',
        },
        'lineaServicioProductoList': {
          'stock': 0,
          'lineaServicioProductoList': 'idCategoriaServicio',
        },
      }
    ].toString()));
  }
}
