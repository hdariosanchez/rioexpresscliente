import 'dart:async';
import 'package:rioexpresscliente/models/factura.model.dart';
import 'package:rioexpresscliente/resources/factura.api.provider.dart';

class RepositoryFactura {
  final facturaApiProvider = FacturaApiProvider();

  Future<FacturaModelList> fetchfetchAllProductoOrdenByNegocio(
          String idCliente) =>
      facturaApiProvider.fetchMyFactura(idCliente);

  Future<bool> ingresoNuevaFactura(Factura jsonFactura) =>
      facturaApiProvider.createMyFactura(jsonFactura);
}
