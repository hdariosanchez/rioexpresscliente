import 'dart:async';
import 'dart:convert';
//import 'package:flutter/foundation.dart';
import 'package:http/http.dart' show Client, Response;
import 'package:rioexpresscliente/models/ordenFactura.model.dart';
import 'package:rioexpresscliente/configuration/local.dart';

class OrdenFacturaApiProvider {
  Client client = Client();
  static IpConfig local = new IpConfig("192.168.1.15", "5000");
  //static IpConfig local = new IpConfig("35.247.235.167", "5000");
  final _baseUrl = "http://${local.getIp()}:${local.getPort()}/api/mongo_v1";

  Future<OrdenFacturaModelList> fetchOrdenes(String idUser) async {
    Response response;
    try {
      response = await client.get("$_baseUrl/factura/all/$idUser");
      print(
          "que llego en el statusCode----->--->__><_<->-${response.statusCode}");
      if (response.statusCode == 200) {
        //return NegocioModel.fromJson(json.decode(response.body));
        return OrdenFacturaModelList.fromJson(json.decode(response.body));
        //return compute(parseData, response.body);
      } else {
        throw new Exception('Fallo cargar los modelos solicitados');
      }
    } catch (Exception) {
      print("Error de conexion con el server" + Exception.toString());
    }
    return null;
  }

  Future<bool> deleteMyDirection(String id) async {
    Response response;
    try {
      response = await client.delete("$_baseUrl/misLugares/app/cliente/$id");

      if (response.statusCode == 200) {
        //return NegocioModel.fromJson(json.decode(response.body));
        return true;
        //return compute(parseData, response.body);
      } else {
        throw new Exception('Fallo cargar los modelos solicitados');
      }
    } catch (Exception) {
      print("Error de conexion con el server" + Exception.toString());
    }
    return false;
  }
}
