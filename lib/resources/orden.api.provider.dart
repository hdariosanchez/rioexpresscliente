import 'dart:async';
import 'dart:convert';
//import 'package:flutter/foundation.dart';
import 'package:http/http.dart' show Client, Response;
import 'package:rioexpresscliente/models/negocio.model.dart';
//import 'package:rioexpresscliente/models/producto.model.dart';
import 'package:rioexpresscliente/models/orden.model.dart';
import 'package:rioexpresscliente/configuration/local.dart';
import 'package:rioexpresscliente/models/producto.model.dart';

class OrdenApiProvider {
  Client client = Client();
  static IpConfig local = new IpConfig("192.168.1.15", "5000");
  //static IpConfig local = new IpConfig("35.247.235.167", "5000");
  final _baseUrl = "http://${local.getIp()}:${local.getPort()}/api/mongo_v1";

  Future<NegocioModelList> fetchAllNegocio() async {
    //Future<List<NegocioModel>> fetchAllNegocio() async {
    Response response;
    try {
      response = await client.get("$_baseUrl/negocio/all");
      if (response.statusCode == 200) {
        //return NegocioModel.fromJson(json.decode(response.body));
        return NegocioModelList.fromJson(json.decode(response.body));
        //return compute(parseData, response.body);
      } else {
        throw new Exception('Fallo cargar los negocios solicitados');
      }
    } catch (Exception) {
      print("Error de conexion con el server" + Exception.toString());
    }

    return NegocioModelList.fromJson(json.decode([
      {
        '_id': '5f07fc7378d0720a2e259f08',
        'urlImagen': '1',
        'nombreCorto': 'Lo sentimos',
        'descripcionLocal': 'Al parecer no cuentas con internet'
      }
    ].toString()));
  }

  Future<OrdenModel> fetchAllProductoCarByNegocio(String idCliente) async {
    var pr = json.encode({});
    Response response;
    try {
      response = await client.get("$_baseUrl/ordenProducto/app/$idCliente");

      if (response.statusCode == 201) {
        var obj = json.decode(response.body);
        print("Viendo que llego en el orden.api que llego --> $obj");
        return OrdenModel.fromJson(obj);
      } else {
        //throw new Exception('Fallo cargar los negocios solicitados');
        return null;
      }
    } catch (Exception) {
      print("Error de conexion con el server" + Exception.toString());
    }
    return null;
  }

  //CREAR NUEVA ORDEN

  Future<String> createNewOrden(OrdenModel newOrden) async {
    Response response;
    try {
      response = await client.post(
        "$_baseUrl/ordenProducto/app",
        headers: {"content-type": "application/json"},
        body: ordenToJson(newOrden),
      );

      if (response.statusCode == 201) {
        var obj = json.decode(response.body);
        return obj["_id"];
      } else {
        throw new Exception(
            'Fallo cargar el ingreso nuevo de la orden  solicitados');
      }
    } catch (Exception) {
      print("Error de conexion con el server" + Exception.toString());
    }
    return null;
  }

  //CREAR NUEVO PRODUCTO ORDEN

  Future<bool> createNewProductoOrden(
      ProductoModel newProduct, String idOrden) async {
    Response response;
    try {
      response = await client.post(
        "$_baseUrl/ordenProducto/producto/app/$idOrden",
        headers: {"content-type": "application/json"},
        body: productoToJson(newProduct),
      );

      if (response.statusCode == 200) {
        return true;
      } else {
        throw new Exception(
            'Fallo cargar el ingreso nuevo de la orden  solicitados');
      }
    } catch (Exception) {
      print("Error de conexion con el server" + Exception.toString());
    }
    return false;
  }
}
