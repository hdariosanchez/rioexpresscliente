import 'dart:async';
import 'package:rioexpresscliente/models/negocio.model.dart';
import 'package:rioexpresscliente/models/producto.model.dart';
import 'package:rioexpresscliente/resources/negocio.api.provider.dart';

class RepositoryNegocio {
  final negocioApiProvider = NegocioApiProvider();
  Future<NegocioModelList> fetchAllNegocio(String tipoNegocio) =>
      negocioApiProvider.fetchAllNegocio(tipoNegocio);

  Future<ProductoModelList> fetchAllProductoByNegocio(String idNegocio) =>
      negocioApiProvider.fetchAllProductoByNegocio(idNegocio);
}
