import 'dart:async';
import 'package:rioexpresscliente/models/ordenFactura.model.dart';
import 'package:rioexpresscliente/resources/ordenesFactura.api.provider.dart';

class RepositoryOrdenesFactura {
  final ordenApiProvider = OrdenFacturaApiProvider();

  Future<OrdenFacturaModelList> fetchOrdenes(String idUser) =>
      ordenApiProvider.fetchOrdenes(idUser);

  Future<bool> deleteMyDirection(String id) =>
      ordenApiProvider.deleteMyDirection(id);
}
