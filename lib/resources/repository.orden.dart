import 'dart:async';
import 'package:rioexpresscliente/models/orden.model.dart';
import 'package:rioexpresscliente/models/producto.model.dart';
//import 'package:rioexpresscliente/models/producto.model.dart';
import 'package:rioexpresscliente/resources/orden.api.provider.dart';

class RepositoryOrden {
  final ordenApiProvider = OrdenApiProvider();
  /*
  Future<NegocioModelList> fetchAllNegocio() =>
      ordenApiProvider.fetchAllNegocio();
      */

  Future<OrdenModel> fetchAllProductoOrdenByNegocio(String idCliente) =>
      ordenApiProvider.fetchAllProductoCarByNegocio(idCliente);

  Future<String> ingresoNuevaOrden(OrdenModel jsonNuevaOrden) =>
      ordenApiProvider.createNewOrden(jsonNuevaOrden);

  Future<bool> ingresoNuevoProductoOrden(
          ProductoModel jsonNuevoProductoOrden, String idOrden) =>
      ordenApiProvider.createNewProductoOrden(jsonNuevoProductoOrden, idOrden);
}
