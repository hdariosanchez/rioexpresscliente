import 'dart:async';
import 'dart:convert';
//import 'package:flutter/foundation.dart';
import 'package:http/http.dart' show Client, Response;
import 'package:rioexpresscliente/models/direccion.model.dart';
import 'package:rioexpresscliente/configuration/local.dart';

class MiDireccionApiProvider {
  Client client = Client();
  static IpConfig local = new IpConfig("192.168.1.15", "5000");
  //static IpConfig local = new IpConfig("35.247.235.167", "5000");
  final _baseUrl = "http://${local.getIp()}:${local.getPort()}/api/mongo_v1";

  Future<MiDireccionModelList> fetchMyDirection(String idCliente) async {
    Response response;
    try {
      response = await client.get("$_baseUrl/misLugares/$idCliente");

      if (response.statusCode == 200) {
        //return NegocioModel.fromJson(json.decode(response.body));
        var res = json.decode(response.body);
        print("que llega en la direccion $res");
        return MiDireccionModelList.fromJson(json.decode(response.body));
        //return compute(parseData, response.body);
      } else {
        throw new Exception('Fallo cargar los modelos solicitados');
      }
    } catch (Exception) {
      print("Error de conexion con el server" + Exception.toString());
    }
    return MiDireccionModelList.fromJson(json.decode([
      {
        '_id': '5f07fc7378d0720a2e259f08',
        'urlImagen': '1',
        'nombreCorto': 'Lo sentimos',
        'nombre': 'Al parecer no cuentas con internet'
      }
    ].toString()));
  }

  Future<bool> deleteMyDirection(String id) async {
    Response response;
    try {
      response = await client.delete("$_baseUrl/misLugares/app/cliente/$id");

      if (response.statusCode == 200) {
        //return NegocioModel.fromJson(json.decode(response.body));
        return true;
        //return compute(parseData, response.body);
      } else {
        throw new Exception('Fallo cargar los modelos solicitados');
      }
    } catch (Exception) {
      print("Error de conexion con el server" + Exception.toString());
    }
    return false;
  }

  Future<bool> createMyDirection(MiDireccion miDireccion) async {
    Response response;
    try {
      response = await client.post(
        "$_baseUrl/misLugares/",
        headers: {"content-type": "application/json"},
        body: misLugaresToJson(miDireccion),
      );

      if (response.statusCode == 200) {
        //return NegocioModel.fromJson(json.decode(response.body));
        return true;
        //return compute(parseData, response.body);
      } else {
        throw new Exception('Fallo cargar los modelos solicitados');
      }
    } catch (Exception) {
      print("Error de conexion con el server" + Exception.toString());
    }
    return false;
  }
}
