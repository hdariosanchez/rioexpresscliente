import 'dart:async';
import 'package:rioexpresscliente/models/direccion.model.dart';
import 'package:rioexpresscliente/resources/direccion.api.provider.dart';

class RepositoryMiDireccion {
  final miDireccionApiProvider = MiDireccionApiProvider();

  Future<MiDireccionModelList> fetchMyDirection(String idCliente) =>
      miDireccionApiProvider.fetchMyDirection(idCliente);

  Future<bool> deleteMyDirection(String id) =>
      miDireccionApiProvider.deleteMyDirection(id);

  Future<bool> createMyDirection(MiDireccion miDireccion) =>
      miDireccionApiProvider.createMyDirection(miDireccion);
}
